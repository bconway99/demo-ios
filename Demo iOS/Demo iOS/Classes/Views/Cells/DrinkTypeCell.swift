//
//  DrinkTypeCell.swift
//  Demo iOS
//
//  Created by Ben Conway on 27/06/2018.
//  Copyright © 2018 Ben Conway. All rights reserved.
//

import UIKit

class DrinkTypeCell: UICollectionViewCell {
    
    static let theWidth: CGFloat = 100.0
    static let theHeight: CGFloat = 40.0
    
    var txtLabel = UILabel()
    
    // MARK: - Init
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        // Initialization code
    }
    
    // Called by the collection view just before this cell instance is returned from the reuse queue and about to be reused.
    override func prepareForReuse() {
        
        super.prepareForReuse()
        
        // Use this method to clear out any existing data as this cell is about to be reused and we don't want to use the prveious instance's data.
        txtLabel.text = nil
    }
    
    // MARK: - Setup
    
    func configureCell(idx: Int, productTxt: String) {
        
        // Text label.
        
        txtLabel.frame = CGRect(x: 0.0, y: 0.0, width: DrinkTypeCell.theWidth, height: DrinkTypeCell.theHeight)
        txtLabel.text = productTxt
        txtLabel.textColor = Colours.Generics.lightText
        txtLabel.numberOfLines = 0
        addSubview(txtLabel)
    }
}
