//
//  SearchResultCell.swift
//  Demo iOS
//
//  Created by Ben Conway on 03/04/2018.
//  Copyright © 2018 Ben Conway. All rights reserved.
//

import UIKit

class SearchResultCell: UIView {

    static let theHeight: CGFloat = 140.0
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var detailsCont: UIView!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    
    // MARK: - Init
    
    // For using custom view in code.
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        setup()
    }
    
    // For using custom view in IB (Interface Builder).
    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
        setup()
    }
    
    func loadViewFromNib() -> UIView! {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
    
    // MARK: - Setup
    
    func setup() {
        
        // Add content view.
        
        contentView = loadViewFromNib()
        contentView.frame = bounds
        contentView.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        addSubview(contentView)
    }
    
    // MARK: - Data
    
    func setData(pub: Pub?, img: UIImage?) {
        
        if imgView != nil && img != nil {
            imgView.image = img
        }
        
        if pub != nil && pub!.pubName != nil {
            nameLabel.text = pub!.pubName!
        }
        
        if pub != nil && pub!.pubTime != nil {
            timeLabel.text = pub!.pubTime!
        }
        
        if pub != nil && pub!.pubName != nil {
            priceLabel.text = "£0.00"
        }
        
        distanceLabel.text = "\(pub!.pubDistance)"
    }
}
