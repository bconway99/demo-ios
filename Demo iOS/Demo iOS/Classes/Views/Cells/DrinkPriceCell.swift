//
//  DrinkPriceCell.swift
//  Demo iOS
//
//  Created by Ben Conway on 28/06/2018.
//  Copyright © 2018 Ben Conway. All rights reserved.
//

import UIKit

class DrinkPriceCell: UIView, UITableViewDataSource, UITableViewDelegate {

    static let mainHeight: CGFloat = 24.0
    static let priceHeight: CGFloat = 20.0
    var pubDrink: PubDrink?
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var priceTable: UITableView!

    // MARK: - Init
    
    // For using custom view in code.
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        setup()
    }
    
    // For using custom view in IB (Interface Builder).
    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
        setup()
    }
    
    func loadViewFromNib() -> UIView! {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
    
    // MARK: - Setup
    
    func setup() {
        
        // Add content view.
        
        contentView = loadViewFromNib()
        contentView.frame = bounds
        contentView.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        addSubview(contentView)
    }
    
    // MARK: - Data
    
    func configureCell(thePubDrink: PubDrink) {
        
        pubDrink = thePubDrink
        if pubDrink!.name != nil {
            nameLabel.text = pubDrink!.name!
        }
        
        priceTable.backgroundColor = UIColor.orange
    }
    
    // MARK: - UI Table View Delegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        guard let prices = pubDrink?.prices else {
            return 0
        }
        return prices.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if pubDrink?.prices == nil || pubDrink!.prices!.count < 1 {
            return 0.0
        }
        return DrinkPriceCell.priceHeight
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = "cellIdentifier"
        var cell: UITableViewCell? = nil
        if cell == nil {
            cell = UITableViewCell(style: .subtitle, reuseIdentifier: cellIdentifier)
        }
        cell?.selectionStyle = .none
        cell?.clipsToBounds = true
        
        if pubDrink != nil && pubDrink!.prices != nil && pubDrink!.prices!.count > 0 {
            
            let pubPrice: PubPrice? = pubDrink!.prices![indexPath.row]
            if pubPrice != nil && pubPrice!.measurement != nil && pubPrice!.price != nil {
                
                let measurementCell = MeasurementCell(frame: CGRect(
                    x: 0.0,
                    y: 0.0,
                    width: priceTable.frame.size.width,
                    height: DrinkPriceCell.priceHeight
                ))
                measurementCell.setData(measurement: pubPrice!.measurement!, price: pubPrice!.price!)
                cell?.contentView.addSubview(measurementCell)
            }
        }
                
        if cell == nil {
            return UITableViewCell()
        } else {
            return cell!
        }
    }
    
    // MARK: - Enable / Disable
    
    func enable() {
        
        contentView.alpha = 1.0
    }
    
    func disable() {
        
        contentView.alpha = 0.5
    }
}
