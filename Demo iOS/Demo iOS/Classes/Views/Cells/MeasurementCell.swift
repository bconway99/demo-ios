//
//  MeasurementCell.swift
//  Demo iOS
//
//  Created by Ben Conway on 01/07/2018.
//  Copyright © 2018 Ben Conway. All rights reserved.
//

import UIKit

class MeasurementCell: UIView {
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var measurementLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!

    // MARK: - Init
    
    // For using custom view in code.
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        setup()
    }
    
    // For using custom view in IB (Interface Builder).
    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
        setup()
    }
    
    func loadViewFromNib() -> UIView! {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
    
    // MARK: - Setup
    
    func setup() {
        
        // Add content view.
        
        contentView = loadViewFromNib()
        contentView.frame = bounds
        contentView.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        addSubview(contentView)
    }
    
    // MARK: - Data
    
    func setData(measurement: String?, price: Double?) {
 
        if measurement != nil {
            measurementLabel.text = measurement!
        }
        
        if price != nil {
            priceLabel.text = "\(price!)"
        }
    }
}
