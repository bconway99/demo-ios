//
//  SearchPubCell.swift
//  Demo iOS
//
//  Created by Ben Conway on 16/07/2018.
//  Copyright © 2018 Ben Conway. All rights reserved.
//

import UIKit

class SearchPubCell: UICollectionViewCell {
    
    var txtLabel = UILabel()
    var imgView = UIImageView()

    // MARK: - Init
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        // Initialization code
    }
    
    // Called by the collection view just before this cell instance is returned from the reuse queue and about to be reused.
    override func prepareForReuse() {
        
        super.prepareForReuse()
        
        // Use this method to clear out any existing data as this cell is about to be reused and we don't want to use the prveious instance's data.
        txtLabel.text = nil
    }
    
    // MARK: - Setup
    
    func configureCell(idx: Int, pub: Pub) {
        
        // Image view.
        
        imgView.frame = CGRect(x: 0.0, y: 0.0, width: frame.size.width, height: frame.size.height)
        imgView.image = UIImage(named: "pub_placeholder")
        imgView.contentMode = .scaleAspectFill
        addSubview(imgView)
        
        // Text label.
        
        txtLabel.frame = CGRect(
            x: 20.0,
            y: 20.0,
            width: frame.size.width - 20.0,
            height: 20.0
        )
        txtLabel.text = pub.pubName != nil ? pub.pubName! : Strings.General.na
        txtLabel.textColor = Colours.Generics.lightText
        txtLabel.numberOfLines = 0
        txtLabel.backgroundColor = Colours.Custom.black5
        addSubview(txtLabel)
    }
}
