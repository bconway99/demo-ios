//
//  AutoCompTable.swift
//  Demo iOS
//
//  Created by Ben Conway on 25/04/2018.
//  Copyright © 2018 Ben Conway. All rights reserved.
//

import UIKit

protocol AutoCompTableDelegate: class {
    
    func resultSelected(result: DrinkAutoResp)
}

class AutoCompTable: UIView, UITableViewDataSource, UITableViewDelegate {

    weak var delegate: AutoCompTableDelegate?
    static let cellHeight: CGFloat = 40.0
    let maxShowCount: Int = 10 // The maximum number of results to be shown, before having to scroll the table.
    var theResults: [DrinkAutoResp]?
    var isOpen: Bool = false

    @IBOutlet var contentView: UIView!
    @IBOutlet weak var resultsTable: UITableView!
    
    // MARK: - Init
    
    // For using custom view in code.
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        setup()
    }
    
    // For using custom view in IB (Interface Builder).
    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
        setup()
    }
    
    func loadViewFromNib() -> UIView! {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
    
    // MARK: - Setup
    
    func setup() {
        
        // Add content view.
        
        contentView = loadViewFromNib()
        contentView.frame = bounds
        contentView.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        addSubview(contentView)
    }
    
    func setData(results: [DrinkAutoResp]) {
        
        theResults = results
        resultsTable.reloadData()
        show()
    }
    
    // MARK: - UI Table View Delegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if theResults == nil {
            return 0
        } else {
            return theResults!.count
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if theResults == nil {
            return 0.0
        } else {
            return AutoCompTable.cellHeight
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = "cellIdentifier"
        var cell: UITableViewCell? = nil
        if cell == nil {
            cell = UITableViewCell(style: .subtitle, reuseIdentifier: cellIdentifier)
        }
        cell?.selectionStyle = .none
        cell?.clipsToBounds = true
        cell?.backgroundColor = Colours.Generics.lightGrey
        
        if theResults != nil && indexPath.row < theResults!.count {
            
            let result: DrinkAutoResp = theResults![indexPath.row]
            if result.drinkName != nil {
                cell?.textLabel?.text = result.drinkName!
            }
        }
        
        if cell == nil {
            return UITableViewCell()
        } else {
            return cell!
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if theResults != nil && indexPath.row < theResults!.count {

            let result: DrinkAutoResp = theResults![indexPath.row]
            if result.drinkName != nil {
                
                if delegate != nil {
                    delegate!.resultSelected(result: result)
                    hide()
                }
            }
        }
    }
    
    // MARK: - Show / Hide
    
    func show() {
        
        if theResults != nil && theResults!.count > 0 {
            
            // Change the views height via it's constraint.
            for constraint in constraints {
                if constraint.identifier == "autoCompHeight" {
                    
                    // Put a maximum height on the table, before having to scroll the it.
                    // Incase we have too many results and the table goes off screen.
                    if theResults!.count >= maxShowCount {
                        constraint.constant = CGFloat(maxShowCount) * AutoCompTable.cellHeight
                    } else {
                        constraint.constant = CGFloat(theResults!.count) * AutoCompTable.cellHeight
                    }
                    isOpen = true
                }
            }
        }
    }
    
    func hide() {
        
        // Change the views height via it's constraint.
        for constraint in constraints {
            if constraint.identifier == "autoCompHeight" {
                constraint.constant = 0.0
                isOpen = false
            }
        }
    }
}
