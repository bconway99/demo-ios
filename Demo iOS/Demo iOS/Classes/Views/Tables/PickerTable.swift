//
//  PickerTable.swift
//  Demo iOS
//
//  Created by Ben Conway on 29/04/2018.
//  Copyright © 2018 Ben Conway. All rights reserved.
//

import UIKit

protocol PickerTableDelegate: class {
    
    func dataSelected(data: String)
}

class PickerTable: UIView, UITableViewDataSource, UITableViewDelegate {
    
    weak var delegate: PickerTableDelegate?
    static let cellHeight: CGFloat = 40.0
    var theData: [String]?
    var isOpen: Bool = false
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var dataTable: UITableView!
    
    // MARK: - Init
    
    // For using custom view in code.
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        setup()
    }
    
    // For using custom view in IB (Interface Builder).
    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
        setup()
    }
    
    func loadViewFromNib() -> UIView! {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
    
    // MARK: - Setup
    
    func setup() {
        
        // Add content view.
        
        contentView = loadViewFromNib()
        contentView.frame = bounds
        contentView.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        addSubview(contentView)
    }
    
    func setData(data: [String]) {
        
        theData = data
    }
    
    // MARK: - UI Table View Delegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if theData == nil {
            return 0
        } else {
            return theData!.count
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if theData == nil {
            return 0.0
        } else {
            return PickerTable.cellHeight
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = "cellIdentifier"
        var cell: UITableViewCell? = nil
        if cell == nil {
            cell = UITableViewCell(style: .subtitle, reuseIdentifier: cellIdentifier)
        }
        cell?.selectionStyle = .none
        cell?.clipsToBounds = true
        cell?.backgroundColor = Colours.Generics.lightGrey
        if theData != nil && indexPath.row < theData!.count {
            cell?.textLabel?.text = theData![indexPath.row].capitalized
        }
        if cell == nil {
            return UITableViewCell()
        } else {
            return cell!
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if theData != nil && indexPath.row < theData!.count {
            
            let selData: String = theData![indexPath.row]
            if delegate != nil {
                delegate!.dataSelected(data: selData)
                hide()
            }
        }
    }
    
    // MARK: - Show / Hide
    
    func show() {
        
        if theData != nil && theData!.count > 0 {
            
            // Change the views height via it's constraint.
            for constraint in constraints {
                if constraint.identifier == "pickerTableHeight" {
                    constraint.constant = CGFloat(theData!.count) * PickerTable.cellHeight
                    dataTable.reloadData()
                    isOpen = true
                }
            }
        }
    }
    
    func hide() {
        
        // Change the views height via it's constraint.
        for constraint in constraints {
            if constraint.identifier == "pickerTableHeight" {
                constraint.constant = 0.0
                isOpen = false
            }
        }
    }
}
