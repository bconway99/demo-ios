//
//  PubResultsScroll.swift
//  Demo iOS
//
//  Created by Ben Conway on 28/07/2018.
//  Copyright © 2018 Ben Conway. All rights reserved.
//

import UIKit

protocol PubResultsScrollDelegate: class {
    
    func pubSel(idx: Int, pub: Pub)
    func snapPub(idx: Int, pub: Pub)
}

class PubResultsScroll: UIView, UICollectionViewDataSource, UICollectionViewDelegate {
    
    weak var delegate: PubResultsScrollDelegate?
    let cellIdentifier: String = "cellIdentifier"
    var pubs: [Pub]?
    var mapVC: MapVC?
    var collectionViewFlowLayout: UICollectionViewFlowLayout?
    var mapMoving: Bool = false // Is the user currently moving the map or not.
    var isHorizontal: Bool = true // Is the collection view displaying horizontally or vertically.
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    // MARK: - Init
    
    // For using custom view in code.
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        setup()
    }
    
    // For using custom view in IB (Interface Builder).
    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
        setup()
    }
    
    func loadViewFromNib() -> UIView! {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
    
    // MARK: - Setup
    
    func setup() {
        
        // Add content view.
        
        contentView = loadViewFromNib()
        contentView.frame = bounds
        contentView.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        addSubview(contentView)
        
        // Collection view layout.
        
        collectionViewFlowLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout
        
        // Switch button.
        
        let switchBtn = UIButton(frame: CGRect(x: 0.0, y: 0.0, width: 40.0, height: 40.0))
        switchBtn.backgroundColor = UIColor.green
        switchBtn.addTarget(self, action: #selector(switchBtnPressed), for: .touchUpInside)
        contentView.addSubview(switchBtn)
    }
    
    func setupCollectionView(thePubs: [Pub]?, theMapVC: MapVC?) {
        
        if thePubs != nil {
            pubs = thePubs!
        }
        
        if theMapVC != nil {
            mapVC = theMapVC!
        }
        
        // Collection view.
        
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(SearchPubCell.self, forCellWithReuseIdentifier: cellIdentifier)
        collectionView.showsHorizontalScrollIndicator = false
        
        let itemSize = CGSize(width: frame.size.width, height: contentView.frame.size.height)
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.sectionInset = UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0)
        layout.itemSize = CGSize(width: itemSize.width, height: itemSize.height)
        layout.minimumInteritemSpacing = 10.0
        layout.minimumLineSpacing = 10.0
        collectionView.collectionViewLayout = layout
        collectionView.reloadData()
    }
    
    func switchDirection(horizontal: Bool, theMapVC: MapVC?) {

        isHorizontal = horizontal
        if theMapVC != nil {
            mapVC = theMapVC!
        }
        
        // If the scroll is changing to vertical then change it's height to match the parent view.
        if isHorizontal == false {
            
            for constraint in constraints {
                if constraint.identifier == "height" {
                    constraint.constant = mapVC!.mapView.frame.size.height + frame.size.height
                    break
                }
            }
            
        } else {
            
            for constraint in constraints {
                if constraint.identifier == "height" {
                    constraint.constant = 200.0
                    break
                }
            }
        }
        

        // Change the scroll direction of the collection view layout.
        if let flowLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            
            flowLayout.scrollDirection = isHorizontal == true ? .horizontal : .vertical
            flowLayout.minimumInteritemSpacing = isHorizontal == true ? 10.0 : 40.0
            flowLayout.minimumLineSpacing = isHorizontal == true ? 10.0 : 30.0
            collectionView.collectionViewLayout = flowLayout
        }
    }
    
    func snapToNearestCell() {
        
        if isHorizontal == true {
            
            let visibleCenterPositionOfScrollView = Float(collectionView.contentOffset.x + (collectionView!.bounds.size.width / 2.0))
            var closestCellIdx = -1
            var closestDistance: Float = .greatestFiniteMagnitude
            for i in 0..<collectionView.visibleCells.count {
                
                let cell = collectionView.visibleCells[i]
                let cellWidth = cell.bounds.size.width
                let cellCenter = Float(cell.frame.origin.x + cellWidth / 2)
                
                // Now calculate closest cell.
                let distance: Float = fabsf(visibleCenterPositionOfScrollView - cellCenter)
                if distance < closestDistance {
                    closestDistance = distance
                    closestCellIdx = collectionView.indexPath(for: cell)!.row
                }
            }
            
            if closestCellIdx != -1 {
                
                collectionView!.scrollToItem(at: IndexPath(row: closestCellIdx, section: 0), at: .centeredHorizontally, animated: true)
                
                // Delay the cantering of the map on the new pub.
                weak var weakSelf = self
                let delaySecs = DispatchTime.now() + 0.5
                DispatchQueue.main.asyncAfter(deadline: delaySecs) {
                    
                    if weakSelf!.mapMoving == false {
                        if weakSelf!.pubs != nil && closestCellIdx < weakSelf!.pubs!.count {
                            if weakSelf!.delegate != nil {
                                weakSelf!.delegate!.snapPub(idx: closestCellIdx, pub: weakSelf!.pubs![closestCellIdx])
                            }
                        }
                    }
                }
            }
        }
    }
    
    func scrollToCell(idxPath: IndexPath) {
        
        if collectionView != nil {
            collectionView!.scrollToItem(at: idxPath, at: .centeredHorizontally, animated: true)
        }
    }
    
    // MARK: - Buttons
    
    @objc func switchBtnPressed() {
        
        if mapVC != nil {
            
            if isHorizontal == true {
                isHorizontal = false
            } else {
                isHorizontal = true
            }
            
            switchDirection(horizontal: isHorizontal, theMapVC: mapVC!)
        }
    }
    
    // MARK: - UI Collection View Delegate
    
    // Tell the collection view how many cells to make.
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if pubs == nil || pubs!.count < 1 {
            return 0
        } else {
            return pubs!.count
        }
    }
    
    // Make a cell for each cell index path.
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! SearchPubCell
        cell.backgroundColor = Colours.Random.getRandomColor(alpha: 1.0)
        
        if pubs != nil && indexPath.row < pubs!.count {
            let pub: Pub? = pubs![indexPath.row]
            cell.configureCell(idx: indexPath.row, pub: pub!)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if pubs != nil || indexPath.row < pubs!.count && mapVC != nil {
            if delegate != nil {
                delegate!.pubSel(idx: indexPath.row, pub: pubs![indexPath.row])
            }
        }
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        
        mapMoving = true
    }
    
    func scrollViewWillBeginDecelerating(_ scrollView: UIScrollView) {
        
        mapMoving = true
    }
    
    func scrollViewWillBeginZooming(_ scrollView: UIScrollView, with view: UIView?) {
        
        mapMoving = true
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        mapMoving = false
        snapToNearestCell()
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        mapMoving = false
        snapToNearestCell()
    }
}
