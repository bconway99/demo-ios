//
//  DrinkHoriScroll.swift
//  Demo iOS
//
//  Created by Ben Conway on 27/06/2018.
//  Copyright © 2018 Ben Conway. All rights reserved.
//

import UIKit

protocol DrinkHoriScrollDelegate: class {
    
    func drinkTypeSel(idx: Int, drinkType: PubDrinkType)
}

class DrinkHoriScroll: UIView, UICollectionViewDataSource, UICollectionViewDelegate {
    
    weak var delegate: DrinkHoriScrollDelegate?
    let cellIdentifier: String = "cellIdentifier"
    var drinkTypes: [PubDrinkType]?
    var vc: UIViewController?
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    // MARK: - Init
    
    // For using custom view in code.
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        setup()
    }
    
    // For using custom view in IB (Interface Builder).
    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
        setup()
    }
    
    func loadViewFromNib() -> UIView! {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        return view
    }
    
    // MARK: - Setup
    
    func setup() {
        
        // Add content view.
        
        contentView = loadViewFromNib()
        contentView.frame = bounds
        contentView.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        addSubview(contentView)
    }
    
    func setupCollectionView(theDrinkTypes: [PubDrinkType]?, theVC: UIViewController?) {
        
        if theDrinkTypes != nil {
            drinkTypes = theDrinkTypes!
        }
        
        if theVC != nil {
            vc = theVC!
        }
        
        // Collection view.
        
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(DrinkTypeCell.self, forCellWithReuseIdentifier: cellIdentifier)
        collectionView.showsHorizontalScrollIndicator = false
        
        let itemSize = CGSize(width: DrinkTypeCell.theWidth, height: contentView.frame.size.height)
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.sectionInset = UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0)
        layout.itemSize = CGSize(width: itemSize.width, height: itemSize.height)
        layout.minimumInteritemSpacing = 10.0
        layout.minimumLineSpacing = 10.0
        
        collectionView.collectionViewLayout = layout
    }
    
    // MARK: - UI Collection View Delegate
    
    // Tell the collection view how many cells to make.
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if drinkTypes == nil || drinkTypes!.count < 1 {
            return 0
        } else {
            return drinkTypes!.count
        }
    }
    
    // Make a cell for each cell index path.
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! DrinkTypeCell
        cell.backgroundColor = Colours.Random.getRandomColor(alpha: 1.0)
        
        if drinkTypes != nil && indexPath.row < drinkTypes!.count {
            let drinkType: PubDrinkType? = drinkTypes![indexPath.row]
            if drinkType!.drinkType != nil {
                cell.configureCell(idx: indexPath.row, productTxt: drinkType!.drinkType!)
            }
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if drinkTypes != nil || indexPath.row < drinkTypes!.count && vc != nil {
            if delegate != nil {
                delegate!.drinkTypeSel(idx: indexPath.row, drinkType: drinkTypes![indexPath.row])
            }
        }
    }
}
