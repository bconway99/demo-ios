//
//  SearchMap.swift
//  Demo iOS
//
//  Created by Ben Conway on 15/04/2018.
//  Copyright © 2018 Ben Conway. All rights reserved.
//

import UIKit
import GoogleMaps

protocol SearchMapDelegate: class {
    
    func selectedMarker(mapPub: MapPub)
}

class SearchMap: GMSMapView, GMSMapViewDelegate {

    weak var mapDelegate: SearchMapDelegate?
    static let defaultZoom: Float = 12.0
    var mapPubs: [MapPub]?
    
    // MARK: - Map
    
    func setup() {

        delegate = self
        isMyLocationEnabled = true
        settings.myLocationButton = true
        isBuildingsEnabled = true
        isIndoorEnabled = true
        animate(toZoom: SearchMap.defaultZoom)
        autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }
    
    func centerMap() {
        
        if myLocation != nil {
            
            let newCamera = GMSCameraPosition.camera(
                withLatitude: myLocation!.coordinate.latitude,
                longitude: myLocation!.coordinate.longitude,
                zoom: camera.zoom < 10.0 ? SearchMap.defaultZoom : camera.zoom
            )
            camera = newCamera
        }
    }
    
    func setMap(latitude: Double, longitude: Double) {
        
        let newCamera = GMSCameraPosition.camera(
            withLatitude: latitude,
            longitude: longitude,
            zoom: 16.0
        )
        animate(to: newCamera)
        weak var weakSelf = self
        let delaySecs = DispatchTime.now() + 2.0
        DispatchQueue.main.asyncAfter(deadline: delaySecs) {
            weakSelf!.camera = newCamera
        }
    }
    
    func setPubs(pubs: [Pub]) {
        
        // Clear any existing markers, polylines, etc.
        clear()
        
        if pubs.count > 0 {
            
            // Initialise / reset the map pub data.
            mapPubs = []
            
            // Iterate over all of the pubs and add markers for each.
            for pub in pubs {
                
                let coordinates: [Double]? = Pub.getCoordinates(pub: pub)
                if coordinates != nil && coordinates!.count == 2 {
                    
                    // Create the Google Maps marker.
                    
                    let position = CLLocationCoordinate2D(latitude: coordinates!.last!, longitude: coordinates!.first!)
                    let marker = GMSMarker(position: position)
                    if pub.pubName != nil {
                        marker.title = pub.pubName!
                    }
                    marker.map = self
                    
                    // Add the pub and marker data to a new MapPub struct.
                    let mapPub = MapPub(pub: pub, marker: marker)
                    mapPubs?.append(mapPub)
                }
            }
        }
    }
    
    // MARK: - GMS Map View Delegate
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        
        if mapPubs != nil && mapPubs!.count > 0 {
            
            // Find the first matching marker in the array.
            let mapPub: MapPub? = mapPubs!.first(where: { $0.marker == marker })
            if mapPub != nil {
                
                if mapDelegate != nil {
                    mapDelegate!.selectedMarker(mapPub: mapPub!)
                }
            }
        }
        return true
    }
}
