//
//  AddDrinkVC.swift
//  Demo iOS
//
//  Created by Ben Conway on 08/08/2018.
//  Copyright © 2018 Ben Conway. All rights reserved.
//

import UIKit

class AddDrinkVC: UIViewController, UITextFieldDelegate, AutoCompTableDelegate  {

    var pub: Pub?
    let addDrinkRequest = AddDrinkRequest()
    var prices: [PubPrice]?

    @IBOutlet weak var mainScroll: UIScrollView!
    @IBOutlet weak var nameTxtField: UITextField!
    @IBOutlet weak var categoryTxtField: UITextField!
    @IBOutlet weak var measurementTxtField: UITextField!
    @IBOutlet weak var priceTxtField: UITextField!
    @IBOutlet weak var priceCont: UIView!
    @IBOutlet weak var autoCompTable: AutoCompTable!
    
    // MARK: - Setup
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        if pub?.pubID == nil {
            
            weak var weakSelf = self
            let alert = UIAlertController(title: Strings.Alerts.Titles.error, message: Strings.Alerts.Messages.general, preferredStyle: .alert)
            let ok = UIAlertAction(title: Strings.General.ok.capitalized, style: .cancel) { (action: UIAlertAction) in
                weakSelf!.dismiss(animated: true, completion: nil)
            }
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
            
        } else {
            
            autoCompTable.delegate = self
            nameTxtField.delegate = self
            nameTxtField.addTarget(self, action: #selector(txtFieldChanged), for: .editingChanged)
            categoryTxtField.addTarget(self, action: #selector(txtFieldChanged), for: .editingChanged)
            measurementTxtField.addTarget(self, action: #selector(txtFieldChanged), for: .editingChanged)
            priceTxtField.addTarget(self, action: #selector(txtFieldChanged), for: .editingChanged)
            
            addDrinkRequest.pubID = pub!.pubID!
            prices = [PubPrice(measurement: nil, price: nil)]
            addDrinkRequest.prices = prices
        }
    }

    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        super.touchesBegan(touches, with: event)
        if autoCompTable.isOpen == true {
            autoCompTable.hide()
        }
    }
    
    // MARK: - Text Field
    
    @objc func txtFieldChanged(sender: UITextField) {
        
        switch sender {
        
            // Name.
            
        case nameTxtField:
            
            if sender.text != nil && sender.text!.count > 1 {
                
                // Use the API to get a list of possible auto completed answers.
                weak var weakSelf = self
                TCPAPIHelper.APICalls.SearchAPI.searchDrinkName(drinkName: sender.text!) { (success: Bool, drinkAutoResps: [DrinkAutoResp]?) in
                    
                    if success == true && drinkAutoResps != nil && drinkAutoResps!.count > 0 {
                        weakSelf!.mainScroll.bringSubview(toFront: weakSelf!.autoCompTable)
                        weakSelf!.autoCompTable.setData(results: drinkAutoResps!)
                    } else {
                        weakSelf!.autoCompTable.hide()
                    }
                }
                
            } else {
                
                // Otherwise set the drink back to nil and hide the table.
                addDrinkRequest.drinkID = nil
                addDrinkRequest.drinkName = nil
                autoCompTable.hide()
            }
            
            break
            
            // Measurement.
            
        case measurementTxtField:
            
            if sender.text != nil && sender.text!.count > 1 {
                // Set measurement.
                addDrinkRequest.prices![0].measurement = sender.text!
            } else {
                // Otherwise set the measurement back to nil.
                addDrinkRequest.prices![0].measurement = nil
            }
            
            break
            
            // Price.
            
        case priceTxtField:
            
            if sender.text != nil && sender.text!.count > 1 {
                // Set price.
                addDrinkRequest.prices![0].price = Double(sender.text!)
            } else {
                // Otherwise set the price back to nil.
                addDrinkRequest.prices![0].price = nil
            }
            
            break
            
        default:
            break
        }
    }
    
    // MARK: - UI Text Field Delegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        nameTxtField.resignFirstResponder()
        return true
    }
    
    // MARK: - Buttons
    
    @IBAction func backBtnPressed(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
    }
    @IBAction func deleteBtnPressed(_ sender: Any) {
        
        print("Delete button pressed!")
    }
    
    @IBAction func addBtnPressed(_ sender: Any) {

        if addDrinkRequest.drinkID == nil || addDrinkRequest.drinkName == nil {
            AnimationHelper.shake(view: nameTxtField)
        } else if addDrinkRequest.category == nil {
            AnimationHelper.shake(view: categoryTxtField)
        } else if addDrinkRequest.prices == nil || addDrinkRequest.prices!.count < 1 {
            AnimationHelper.shake(view: priceCont)
        } else {
            
            weak var weakSelf = self
            TCPAPIHelper.APICalls.PubAPI.addDrink(addDrinkRequest: addDrinkRequest) { (success: Bool) in
                
                if success == true {
                    
                } else {
                    
                }
            }
        }
    }
    
    @IBAction func updateBtnPressed(_ sender: Any) {
    }
    
    // MARK: - Auto Comp Table Delegate
    
    func resultSelected(result: DrinkAutoResp) {
        
        if result.drinkID != nil {
            addDrinkRequest.drinkID = result.drinkID!
        }
        if result.drinkName != nil {
            addDrinkRequest.drinkName = result.drinkName!
            nameTxtField.text = result.drinkName!
        }
        if result.drinkType != nil {
            addDrinkRequest.category = result.drinkType!
            categoryTxtField.text = result.drinkType!
        }
    }
}
