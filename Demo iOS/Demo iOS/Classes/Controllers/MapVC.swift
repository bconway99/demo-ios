//
//  MapVC.swift
//  Demo iOS
//
//  Created by Ben Conway on 19/03/2018.
//  Copyright © 2018 Ben Conway. All rights reserved.
//

import UIKit
import CoreLocation
import SlideMenuControllerSwift

class MapVC: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, LocationHelperDelegate, SearchMapDelegate, PubResultsScrollDelegate {

    var slideMenuController: SlideMenuController?
    var searchRequest: PubSearchRequest?
    let locationHelper = LocationHelper()
    var pubs: [Pub]?
    let cellIdentifier: String = "cellIdentifier"

    @IBOutlet weak var mapView: SearchMap!
    @IBOutlet weak var pubResultsScroll: PubResultsScroll!

    // MARK: - Setup
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        mapView.mapDelegate = self
        pubResultsScroll.delegate = self
        
        // Validate the user.
        weak var weakSelf = self
        TCPAPIHelper.APICalls.LoginAPI.authorize { (success: Bool) in
            
            if success == false {
                
                // Validation failed so log the user out.
                LogoutHelper.logout(completionHandler: { (success: Bool) in
                    
                    let destVC = weakSelf!.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as? LoginVC
                    weakSelf!.present(destVC!, animated: true, completion: nil)
                })
                
            } else {
                
                // Validation successful so proceed with setup.
                
                NotificationCenter.default.addObserver(self, selector: #selector(weakSelf!.applicationDidBecomeActive), name: .UIApplicationDidBecomeActive, object: nil)

                if weakSelf!.searchRequest == nil {
                    weakSelf!.searchRequest = PubSearchRequest()
                }
                
                weakSelf!.mapView.setup()
                
                weakSelf!.locationHelper.delegate = self
                if LocationHelper.isAuthorised() == true && LocationHelper.isEnabled() == true {
                    weakSelf!.locationHelper.startUpdating()
                }
            }
        }
    }

    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: .UIApplicationDidBecomeActive, object: nil)
    }
    
    @objc func applicationDidBecomeActive() {
        
        if LocationHelper.isAuthorised() == false || LocationHelper.isEnabled() == false {
            
            // Show an alert for the user to allow their location services.
            let alert = UIAlertController(title: Strings.Alerts.Titles.locNotEnabled.capitalized, message: Strings.Alerts.Messages.allowLocInstrc, preferredStyle: .alert)
            let cancel = UIAlertAction(title: Strings.General.cancel.capitalized, style: .cancel, handler: nil)
            let settings = UIAlertAction(title: Strings.General.settings.capitalized, style: .default) { (action: UIAlertAction) in
                
                // Open the settings app.
                guard let url = URL(string: UIApplicationOpenSettingsURLString) else {
                    return
                }
                if UIApplication.shared.canOpenURL(url) == true {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                }
            }
            alert.addAction(cancel)
            alert.addAction(settings)
            present(alert, animated: true, completion: nil)
        }
    }
    
    // MARK: - Buttons
    
    @IBAction func menuBtnPressed(_ sender: Any) {
        
        if slideMenuController() == nil {
            
            // Show the user the error.
            let alert = UIAlertController(title: Strings.Alerts.Titles.error.capitalized, message: Strings.Alerts.Messages.general, preferredStyle: .alert)
            let ok = UIAlertAction(title: Strings.General.ok.capitalized, style: .cancel, handler: nil)
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
            
        } else {
            
            // Open the menu view controller.
            slideMenuController()!.openLeft()
        }
    }
    
    @IBAction func filterBtnPressed(_ sender: Any) {
        
        let destVC = storyboard?.instantiateViewController(withIdentifier: "SearchVC") as? SearchVC
        destVC!.mapVC = self
        destVC!.searchRequest = searchRequest
        present(destVC!, animated: true, completion: nil)
    }
    
//    // MARK: - Touches
//    
//    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//        
//        super.touchesBegan(touches, with: event)
//        if pubResultsScroll.isHorizontal == true {
//            pubResultsScroll.switchDirection(horizontal: false, theMapVC: self)
//        } else {
//            pubResultsScroll.switchDirection(horizontal: true, theMapVC: self)
//        }
//    }
    
    // MARK: - Location Helper Delegate
    
    func locAuthStatus(enabled: Bool) {
        
        // The location services authorisation status has changed so try and setup a search request.
        if enabled == true {
            if LocationHelper.isAuthorised() == true && LocationHelper.isEnabled() == true {
                locationHelper.startUpdating()
            }
        }
    }
    
    func locationUpdated(coordinate: CLLocationCoordinate2D) {
        
        // Do a default pub search with the user's location.
        // This should only run once.
        
        searchRequest!.distance = 5
        searchRequest!.distanceUnits = Constants.TCPAPI.DistanceUnits.miles
        searchRequest!.setSearchType(str: JSONKeys.SearchTypes.closestPub)
        searchRequest!.addUserLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)
        search(searchRequest: searchRequest!)
    }
    
    func headerUpdated() {
    }
    
    // MARK: - Search
    
    func search(searchRequest: PubSearchRequest) {
        
        weak var weakSelf = self
        TCPAPIHelper.APICalls.SearchAPI.search(searchRequest: searchRequest) { (success: Bool) in
            
            if success == false {
                
                // Show error alert.
                let alert = UIAlertController(title: Strings.Alerts.Titles.error.capitalized, message: Strings.Alerts.Messages.searchFailed, preferredStyle: .alert)
                let ok = UIAlertAction(title: Strings.General.ok.capitalized, style: .cancel, handler: nil)
                alert.addAction(ok)
                weakSelf!.present(alert, animated: true, completion: nil)
                
            } else {
                
                if weakSelf!.pubs != nil && weakSelf!.pubs!.count > 0 {
                    weakSelf!.pubs!.removeAll()
                }
                
                let managedObjs = Pub.mr_findAll()
                if managedObjs == nil || managedObjs!.count < 1 || managedObjs as? [Pub] == nil {
                    
                    // Show error alert.
                    let alert = UIAlertController(title: Strings.Alerts.Titles.error.capitalized, message: Strings.Alerts.Messages.searchFailed, preferredStyle: .alert)
                    let ok = UIAlertAction(title: Strings.General.ok.capitalized, style: .cancel, handler: nil)
                    alert.addAction(ok)
                    weakSelf!.present(alert, animated: true, completion: nil)
                    
                } else {
                    
                    weakSelf!.pubs = managedObjs as? [Pub]
                    if weakSelf!.pubs != nil && weakSelf!.pubs!.count > 0 {

                        weakSelf!.mapView.setPubs(pubs: weakSelf!.pubs!)
                        weakSelf!.mapView.centerMap()
                        weakSelf!.pubResultsScroll.setupCollectionView(thePubs: weakSelf!.pubs!, theMapVC: self)
                    }
                }
            }
        }
    }
    
    // MARK: - Search Map Delegate

    func selectedMarker(mapPub: MapPub) {
        
        if pubs != nil && pubs!.count > 0 {
            
            // Find the first matching marker in the array.
            let pub: Pub? = pubs!.first(where: { $0.pubID == mapPub.pub?.pubID })
            if pub != nil {
                
                // Get the index of the selected pub in the table view.
                guard let idx = pubs!.index(of: pub!) else {
                    return
                }
                let idxPath = IndexPath(row: idx, section: 0)
                pubResultsScroll.scrollToCell(idxPath: idxPath)
            }
        }
    }
    
    // MARK: - UI Collection View Delegate
    
    // Tell the collection view how many cells to make.
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if pubs == nil || pubs!.count < 1 {
            return 0
        } else {
            return pubs!.count
        }
    }
    
    // Make a cell for each cell index path.
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath)
        cell.backgroundColor = Colours.Random.getRandomColor(alpha: 1.0)
        if pubs != nil && indexPath.row < pubs!.count {

            let resultCell = SearchResultCell(frame: CGRect(x: 0.0, y: 0.0, width: pubResultsScroll.frame.size.width, height: pubResultsScroll.frame.size.height))
            resultCell.setData(pub: pubs![indexPath.row], img: nil)
            cell.contentView.addSubview(resultCell)
        }

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        if pubs != nil && indexPath.row < pubs!.count {
            
            let destVC = storyboard?.instantiateViewController(withIdentifier: "PubProfileVC") as? PubProfileVC
            destVC!.pub = pubs![indexPath.row]
            present(destVC!, animated: true, completion: nil)
        }
    }
    
    // MARK: - Pub Results Scroll Delegate
    
    func pubSel(idx: Int, pub: Pub) {
        
        if pubs != nil && idx < pubs!.count {

            let destVC = storyboard?.instantiateViewController(withIdentifier: "PubProfileVC") as? PubProfileVC
            destVC!.pub = pub
            present(destVC!, animated: true, completion: nil)
        }
    }
    
    func snapPub(idx: Int, pub: Pub) {
        
        mapView.setMap(latitude: pub.pubLatitude, longitude: pub.pubLongitude)
    }
}
