//
//  UpdatePubVC.swift
//  Demo iOS
//
//  Created by Ben Conway on 20/05/2018.
//  Copyright © 2018 Ben Conway. All rights reserved.
//

import UIKit

class UpdatePubVC: UIViewController {

    var pub: Pub?
    
    // MARK: - Setup
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        // Do any additional setup after loading the view.

        weak var weakSelf = self

        // Fail-safe.
        if pub == nil || pub!.pubID == nil {

            let alert = UIAlertController(title: Strings.Alerts.Titles.error.capitalized, message: Strings.Alerts.Messages.general, preferredStyle: .alert)
            let ok = UIAlertAction(title: Strings.General.ok.capitalized, style: .cancel) { (action: UIAlertAction) in
                weakSelf!.dismiss(animated: true, completion: nil)
            }
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
        }
        
        // Make the add pub request.
        
        let updatePubRequest = UpdatePubRequest()
        // Assign all of the current pub values into the request before adding new ones.
        // This is because the request requires all of the pubs values, not just the ones being updated.
        updatePubRequest.setCurValues(pub: pub!) { () in
            
            updatePubRequest.pubID = weakSelf!.pub!.pubID!
            updatePubRequest.pubName = "Red Face Hotel"
            updatePubRequest.pubCity = "London"
            updatePubRequest.pubCountry = "United Kingdom"
            TCPAPIHelper.APICalls.PubAPI.updatePub(updatePubRequest: updatePubRequest, completionHandler: { (success: Bool) in
                
                if success == false {
                    
                } else {
                    
                }
            })
        }
    }
    
    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Buttons
    
    @IBAction func backBtnPressed(_ sender: Any) {
    
        dismiss(animated: true, completion: nil)
    }
}
