//
//  MenuVC.swift
//  Demo iOS
//
//  Created by Ben Conway on 25/06/2018.
//  Copyright © 2018 Ben Conway. All rights reserved.
//

import UIKit

class MenuVC: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var menuTable: UITableView!
    
    // MARK: - Setup
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
     
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Log Out
    
    func logOut() {
        
        // Log the user out.
        weak var weakSelf = self
        LogoutHelper.logout(completionHandler: { (success: Bool) in
            
            let destVC = weakSelf!.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as? LoginVC
            weakSelf!.present(destVC!, animated: true, completion: nil)
        })
    }
    
    // MARK: - UI Table View Delegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 3
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 50.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = "cellIdentifier"
        var cell: UITableViewCell? = nil
        if cell == nil {
            cell = UITableViewCell(style: .subtitle, reuseIdentifier: cellIdentifier)
        }
        cell?.selectionStyle = .none
        cell?.clipsToBounds = true

        switch indexPath.row {
            
            // Profile.
            
        case 0:
            cell?.textLabel?.text = Strings.General.profile.capitalized
            break
            
            // Contact us.
            
        case 1:
            cell?.textLabel?.text = Strings.General.contactUs.capitalized
            break
            
            // Log out.
            
        case 2:
            cell?.textLabel?.text = Strings.General.logOut.capitalized
            break
            
        default:
            break
        }
        
        if cell == nil {
            return UITableViewCell()
        } else {
            return cell!
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.row {
            
            // Profile.
            
        case 0:
            break
            
            // Contact us.
            
        case 1:
            break
            
            // Log out.
            
        case 2:
            logOut()
            break
            
        default:
            break
        }
    }
}
