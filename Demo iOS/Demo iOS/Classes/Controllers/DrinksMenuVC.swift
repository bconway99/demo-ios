//
//  DrinksMenuVC.swift
//  Demo iOS
//
//  Created by Ben Conway on 17/06/2018.
//  Copyright © 2018 Ben Conway. All rights reserved.
//

import UIKit

class DrinksMenuVC: UIViewController, UITableViewDataSource, UITableViewDelegate, DrinkHoriScrollDelegate {

    var pub: Pub?
    var drinkTypes: [PubDrinkType]?
    var typeIdx: Int = 0
    
    @IBOutlet weak var drinksTable: UITableView!
    @IBOutlet weak var drinkHoriScroll: DrinkHoriScroll!
    
    // MARK: - Setup
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        drinkHoriScroll.delegate = self
    }

    override func didReceiveMemoryWarning() {
     
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        if drinkTypes != nil && drinkTypes!.count > 0 {
            drinkHoriScroll.setupCollectionView(theDrinkTypes: drinkTypes!, theVC: self)
        }
    }
    
    func getRowHeight(row: Int) -> CGFloat {
        
        guard let pubPrices = drinkTypes?[typeIdx].pubDrinks?[row].prices else {
            return 0.0
        }
        return DrinkPriceCell.mainHeight + (DrinkPriceCell.priceHeight * CGFloat(pubPrices.count))
    }
    
    // MARK: - Buttons
    
    @IBAction func backBtnPressed(_ sender: Any) {
    
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func addBtnPressed(_ sender: Any) {
        
        let destVC = storyboard?.instantiateViewController(withIdentifier: "AddDrinkVC") as? AddDrinkVC
        destVC!.pub = pub!
        present(destVC!, animated: true, completion: nil)
    }
    
    // MARK: - UI Table View Delegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        guard let pubDrinks = drinkTypes?[typeIdx].pubDrinks else {
            return 0
        }
        return pubDrinks.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return getRowHeight(row: indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = "cellIdentifier"
        var cell: UITableViewCell? = nil
        if cell == nil {
            cell = UITableViewCell(style: .subtitle, reuseIdentifier: cellIdentifier)
        }
        cell?.selectionStyle = .none
        cell?.clipsToBounds = true
        
        if drinkTypes != nil && typeIdx < drinkTypes!.count && drinkTypes![typeIdx].pubDrinks != nil {
            
            let pubDrink: PubDrink? = drinkTypes![typeIdx].pubDrinks![indexPath.row]
            if pubDrink != nil {
                
                let drinkPriceCell = DrinkPriceCell(frame: CGRect(
                    x: 0.0,
                    y: 0.0,
                    width: drinksTable.frame.size.width,
                    height: getRowHeight(row: indexPath.row)
                ))
                drinkPriceCell.backgroundColor = Colours.Random.getRandomColor(alpha: 1.0)
                drinkPriceCell.configureCell(thePubDrink: pubDrink!)
                cell?.contentView.addSubview(drinkPriceCell)
            }
        }
        
        if cell == nil {
            return UITableViewCell()
        } else {
            return cell!
        }
    }
    
    // MARK: - Drink Hori Scroll Delegate
    
    func drinkTypeSel(idx: Int, drinkType: PubDrinkType) {
        
        typeIdx = idx
        drinksTable.reloadData()
        print("Sel idx: \(typeIdx)")
    }
}
