//
//  AddPubVC.swift
//  Demo iOS
//
//  Created by Ben Conway on 20/05/2018.
//  Copyright © 2018 Ben Conway. All rights reserved.
//

import UIKit

class AddPubVC: UIViewController {

    // MARK: - Setup
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        // Make the add pub request.
        
        let addPubRequest = AddPubRequest()
        addPubRequest.pubName = "Ben's Amazing Pub"
        addPubRequest.pubAddress = "200 Union Street"
        addPubRequest.pubFranchise = "Puzzle"
        addPubRequest.pubTimes = addPubRequest.addTimes()
        addPubRequest.pubFeatures = ["wifi", "step_free_access", "outdoor_seating"]
        addPubRequest.pubCity = "London"
        addPubRequest.pubCountry = "United Kingdom"
        addPubRequest.pubWebsite = "http://www.newpub.co.uk"
        addPubRequest.pubLatitude = 51.503828
        addPubRequest.pubLongitude = -0.101977
        TCPAPIHelper.APICalls.PubAPI.addPub(addPubRequest: addPubRequest, completionHandler: { (success: Bool) in
            
        })
    }

    override func didReceiveMemoryWarning() {
     
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Buttons
    
    @IBAction func backBtnPressed(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
    }
}
