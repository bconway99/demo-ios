//
//  LoginVC.swift
//  Demo iOS
//
//  Created by Ben Conway on 20/02/2018.
//  Copyright © 2018 Ben Conway. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift

class LoginVC: UIViewController, FacebookHelperDelegate {

    var facebookHelper = FacebookHelper()
    
    @IBOutlet weak var facebookBtn: UIButton!
    
    // MARK: - Setup
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Buttons
    
    @IBAction func facebookBtnPressed(_ sender: Any) {
        
        facebookHelper.delegate = self
        facebookHelper.logIn(vc: self)
    }
    
    // MARK: - Facebook Helper Delegate
    
    func fbLoginSuccess(accessToken: String, dict: [String : Any]) {

        weak var weakSelf = self
        // Use the Facebook access token for Firebase authentication.
        FirebaseHelper.authFacebook(accessToken: accessToken, completionHandler: { (success: Bool, idToken: String?) in
            
            if success == false || idToken == nil {
                
                // Show the user the error.
                let alert = UIAlertController(title: Strings.Alerts.Titles.error.capitalized, message: Strings.Alerts.Messages.loginFailed, preferredStyle: .alert)
                let ok = UIAlertAction(title: Strings.General.ok.capitalized, style: .cancel, handler: nil)
                alert.addAction(ok)
                weakSelf!.present(alert, animated: true, completion: nil)
                
            } else {

                // If the user is authorised successfully then call the login API call.
                TCPAPIHelper.APICalls.LoginAPI.login(token: idToken!) { (success: Bool) in
                    
                    if success == false {
                        
                        // Show the user the error.
                        let alert = UIAlertController(title: Strings.Alerts.Titles.error.capitalized, message: Strings.Alerts.Messages.loginFailed, preferredStyle: .alert)
                        let ok = UIAlertAction(title: Strings.General.ok.capitalized, style: .cancel, handler: nil)
                        alert.addAction(ok)
                        weakSelf!.present(alert, animated: true, completion: nil)
                        
                    } else {
                        
                        // Send the user to the map view, but implement the sliding view controller.
                        let mapVC = weakSelf!.storyboard?.instantiateViewController(withIdentifier: "MapVC") as? MapVC
                        let menuVC = weakSelf!.storyboard?.instantiateViewController(withIdentifier: "MenuVC") as? MenuVC
                        let destVC = SlideMenuController(mainViewController: mapVC!, leftMenuViewController: menuVC!)
                        weakSelf!.present(destVC, animated: true, completion: nil)
                    }
                }
            }
        })
    }
    
    func fbLoginFailed() {
        
        // This method is part of the FacebookHelper delegate, but not currently being used.
    }
    
    func fbLogout() {
        
        // This method is part of the FacebookHelper delegate, but not currently being used.
    }
}

