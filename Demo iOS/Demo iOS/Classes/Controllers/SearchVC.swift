//
//  SearchVC.swift
//  Demo iOS
//
//  Created by Ben Conway on 08/04/2018.
//  Copyright © 2018 Ben Conway. All rights reserved.
//

import UIKit

class SearchVC: UIViewController, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, PickerTableDelegate, AutoCompTableDelegate {

    var mapVC: MapVC?
    let cellHeight: CGFloat = 60.0
    var isMiles: Bool = true // Default to miles.
    var searchRequest: PubSearchRequest?
    
    @IBOutlet weak var mainScroll: UIScrollView!
    @IBOutlet weak var distNumLabel: UILabel!
    @IBOutlet weak var distUnitSegCont: UISegmentedControl!
    @IBOutlet weak var searchTypeBtn: UIButton!
    @IBOutlet weak var distSlider: UISlider!
    @IBOutlet weak var optionsTable: UITableView!
    @IBOutlet weak var drinkTxtField: UITextField!
    @IBOutlet weak var autoCompTable: AutoCompTable!
    @IBOutlet weak var searchTypeTable: PickerTable!
    
    // MARK: - Setup
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        if searchRequest == nil {
            searchRequest = PubSearchRequest()
        }
        
        autoCompTable.delegate = self
        drinkTxtField.delegate = self
        drinkTxtField.addTarget(self, action: #selector(txtFieldChanged), for: .editingChanged)
        
        searchTypeTable.delegate = self
        let searchTypes: [String] = [
        
            Strings.SearchTypes.cheapestPint,
            Strings.SearchTypes.cheapestDrink,
            Strings.SearchTypes.cheapestType,
            Strings.SearchTypes.closestPub
        ]
        searchTypeBtn.setTitle(searchTypes.last!.capitalized, for: .normal)
        searchTypeTable.setData(data: searchTypes)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        // Update the UI with the data from the previously used search request.
        
        // Distance.
        
        // Get the distance value from the passed in search request object.
        var distInt: Int = 0
        if searchRequest!.distance != nil {
            distInt = Int(searchRequest!.distance!)
        }
        distNumLabel.text = "\(distInt)"
        distSlider.value = Float(distInt)
        
        // Drink.
        
        if searchRequest!.drink != nil {
            drinkTxtField.text = searchRequest!.drink!
        }
    }

    override func didReceiveMemoryWarning() {
     
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        super.touchesBegan(touches, with: event)
        searchTypeTable.hide()
        if autoCompTable.isOpen == true {
            autoCompTable.hide()
        }
    }
    
    // MARK: - Slider
    
    func updateDistance() {
        
        // Round the value and convert it to an integer before displaying it.
        let distInt: Int = Int(round(distSlider.value))
        distNumLabel.text = "\(distInt)"
        searchRequest!.distance = distInt
    }
    
    @IBAction func distSliderChanged(_ sender: Any) {
        
        updateDistance()
    }
    
    // MARK: - Text Field
    
    @objc func txtFieldChanged(sender: UITextField) {
        
        if sender.text != nil && sender.text!.count > 1 {
            
            // Use the API to get a list of possible auto completed answers.
            weak var weakSelf = self
            TCPAPIHelper.APICalls.SearchAPI.searchDrinkName(drinkName: sender.text!) { (success: Bool, drinkAutoResps: [DrinkAutoResp]?) in
                
                if success == true && drinkAutoResps != nil && drinkAutoResps!.count > 0 {
                
                    weakSelf!.searchRequest!.drink = sender.text!
                    weakSelf!.mainScroll.bringSubview(toFront: weakSelf!.autoCompTable)
                    weakSelf!.autoCompTable.setData(results: drinkAutoResps!)
                
                } else {
                    weakSelf!.autoCompTable.hide()
                }
            }
        
        } else {
            
            // Otherwise set the drink back to nil and hide the table.
            searchRequest!.drink = nil
            autoCompTable.hide()
        }
    }
    
    // MARK: - UI Text Field Delegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        drinkTxtField.resignFirstResponder()
        return true
    }
    
    // MARK: - Picker Table Delegate
    
    func dataSelected(data: String) {

        searchTypeBtn.setTitle(data.capitalized, for: .normal)
        // Set the search type in the search request.
        searchRequest!.setSearchType(str: data)
    }
    
    // MARK: - Auto Comp Table Delegate
    
    func resultSelected(result: DrinkAutoResp) {
        
        drinkTxtField.text = result.drinkName!
        if result.drinkName != nil {
            searchRequest!.drink = result.drinkName!
        }
        if result.drinkType != nil {
            searchRequest!.drinkType = result.drinkType!
        }
    }
    
    // MARK: - Buttons
    
    @IBAction func backBtnPressed(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func searchBtnPressed(_ sender: Any) {
        
        // Check if the search request has all the minimum fields first.

        // For the CheapestPint and CheapestType search types.
        if searchRequest!.searchType! == .CheapestPint || searchRequest!.searchType! == .CheapestType {
        
            // These search types must have a drink type at least.
            if searchRequest!.drinkType == nil {
                
                // Show error.
                let alert = UIAlertController(title: Strings.Alerts.Titles.error.capitalized, message: Strings.Alerts.Messages.specifyDrink, preferredStyle: .alert)
                let ok = UIAlertAction(title: Strings.General.ok.capitalized, style: .cancel, handler: nil)
                alert.addAction(ok)
                present(alert, animated: true, completion: nil)
                return
            }
        }
        
        // For the CheapestPint and CheapestDrink search type.
        if searchRequest!.searchType! == .CheapestDrink {
            
            // This search type must have a drink name and a drink type at least.
            if searchRequest!.drink == nil || searchRequest!.drinkType == nil {
                
                // Show error.
                let alert = UIAlertController(title: Strings.Alerts.Titles.error.capitalized, message: Strings.Alerts.Messages.specifyDrink, preferredStyle: .alert)
                let ok = UIAlertAction(title: Strings.General.ok.capitalized, style: .cancel, handler: nil)
                alert.addAction(ok)
                present(alert, animated: true, completion: nil)
                return
            }
        }
        
        // The search request has passed all validation checks.
        // Dismiss this view controller and pass the search request into the search method.
        if mapVC != nil {
            mapVC!.searchRequest = searchRequest!
            mapVC!.search(searchRequest: searchRequest!)
        }
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func searchTypeBtnPressed(_ sender: Any) {
        
        // Show or hide a list of available search types.
        if searchTypeTable.isOpen == false {
            searchTypeTable.show()
        } else {
            searchTypeTable.hide()
        }
    }
    
    @IBAction func distUnitSegCont(_ sender: Any) {
        
        if distUnitSegCont.selectedSegmentIndex == 0 {
            isMiles = true
            searchRequest!.distanceUnits = Constants.TCPAPI.DistanceUnits.miles
        } else {
            isMiles = false
            searchRequest!.distanceUnits = Constants.TCPAPI.DistanceUnits.kilometres
        }
    }
    
    @objc func optionSwitchPressed(sender: UISwitch) {
        
        let idx = sender.tag
        switch idx {
            
            // Pool.
            
        case 0:
            searchRequest!.setOption(key: JSONKeys.PubFeatures.pool, on: sender.isOn)
            break
            
            // Food.
            
        case 1:
            searchRequest!.setOption(key: JSONKeys.PubFeatures.food, on: sender.isOn)
            break
            
            // WiFi.
            
        case 2:
            searchRequest!.setOption(key: JSONKeys.PubFeatures.wifi, on: sender.isOn)
            break
            
            // Outdoor seating.
            
        case 3:
            searchRequest!.setOption(key: JSONKeys.PubFeatures.outdoorSeating, on: sender.isOn)
            break
            
            // Family friendly.
            
        case 4:
            searchRequest!.setOption(key: JSONKeys.PubFeatures.familyFriendly, on: sender.isOn)
            break
            
            // Live TV.
            
        case 5:
            searchRequest!.setOption(key: JSONKeys.PubFeatures.liveTV, on: sender.isOn)
            break
            
            // Live sport.
            
        case 6:
            searchRequest!.setOption(key: JSONKeys.PubFeatures.liveSport, on: sender.isOn)
            break
            
            // Live music.
            
        case 7:
            searchRequest!.setOption(key: JSONKeys.PubFeatures.liveMusic, on: sender.isOn)
            break
            
            // Pet friendly.
            
        case 8:
            searchRequest!.setOption(key: JSONKeys.PubFeatures.petFriendly, on: sender.isOn)
            break
            
            // Parking.
            
        case 9:
            searchRequest!.setOption(key: JSONKeys.PubFeatures.parking, on: sender.isOn)
            break
            
            // Step free access.
            
        case 10:
            searchRequest!.setOption(key: JSONKeys.PubFeatures.stepFree, on: sender.isOn)
            break
            
            // Quiz night.
            
        case 11:
            searchRequest!.setOption(key: JSONKeys.PubFeatures.quiz, on: sender.isOn)
            break
            
        default:
            break
        }
    }
    
    // MARK: - UI Table View Delegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 12
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return cellHeight
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = "cellIdentifier"
        var cell: UITableViewCell? = nil
        if cell == nil {
            cell = UITableViewCell(style: .subtitle, reuseIdentifier: cellIdentifier)
        }
        cell?.selectionStyle = .none
        cell?.clipsToBounds = true
        
        // Switch.
        
        let switchSize = CGSize(width: 49.0, height: 31.0)
        let theSwitch = UISwitch(frame: CGRect(
            x: optionsTable.frame.size.width - switchSize.width - 12.0,
            y: cellHeight / 2.0 - (switchSize.height / 2.0),
            width: switchSize.width,
            height: switchSize.height
        ))
        theSwitch.tag = indexPath.row
        theSwitch.addTarget(self, action: #selector(optionSwitchPressed), for: .valueChanged)
        cell?.contentView.addSubview(theSwitch)
        
        switch indexPath.row {
            
            // Pool.
            
        case 0:
            cell?.textLabel?.text = Strings.General.pool.capitalized
            break
            
            // Food.
            
        case 1:
            cell?.textLabel?.text = Strings.General.food.capitalized
            break
            
            // WiFi.
            
        case 2:
            cell?.textLabel?.text = Strings.General.wifi
            break
            
            // Outdoor seating.
            
        case 3:
            cell?.textLabel?.text = Strings.General.outdoorSeating.capitalized
            break
            
            // Family friendly.
            
        case 4:
            cell?.textLabel?.text = Strings.General.familyFriendly.capitalized
            break
            
            // Live TV.
            
        case 5:
            cell?.textLabel?.text = Strings.General.live.capitalized + " " + Strings.General.tv
            break
            
            // Live sport.

        case 6:
            cell?.textLabel?.text = Strings.General.live.capitalized + " " + Strings.General.sport.capitalized
            break
            
            // Live music.
            
        case 7:
            cell?.textLabel?.text = Strings.General.live.capitalized + " " + Strings.General.music.capitalized
            break
            
            // Pet friendly.
            
        case 8:
            cell?.textLabel?.text = Strings.General.petFriendly.capitalized
            break
            
            // Parking.
            
        case 9:
            cell?.textLabel?.text = Strings.General.parking.capitalized
            break
            
            // Step free access.
            
        case 10:
            cell?.textLabel?.text = Strings.General.stepFreeAccess.capitalized
            break
            
            // Quiz night.
            
        case 11:
            cell?.textLabel?.text = Strings.General.quizNight.capitalized
            break
        
        default:
            break
        }
        
        if cell == nil {
            return UITableViewCell()
        } else {
            return cell!
        }
    }
}
