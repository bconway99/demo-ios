//
//  PubProfileVC.swift
//  Demo iOS
//
//  Created by Ben Conway on 30/04/2018.
//  Copyright © 2018 Ben Conway. All rights reserved.
//

import UIKit

class PubProfileVC: UIViewController, UITableViewDataSource, UITableViewDelegate, PhotoHelperDelegate {

    let photoHelper = PhotoHelper()
    var pub: Pub?
    var pubTimes: [PubTimes]?
    var drinkTypes: [PubDrinkType]?
    var pubFeatures: [String]?
    let timesCellHeight: CGFloat = 40.0
    let featuresCellHeight: CGFloat = 25.0

    @IBOutlet weak var pubImgView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var addrLabel: UILabel!
    @IBOutlet weak var contactCont: UIView!
    @IBOutlet weak var favouriteCont: UIView!
    @IBOutlet weak var directionsCont: UIView!
    @IBOutlet weak var addrCont: UIView!
    @IBOutlet weak var timesCont: UIView!
    @IBOutlet weak var featuresCont: UIView!
    @IBOutlet weak var timesTable: UITableView!
    @IBOutlet weak var featuresTable: UITableView!
    
    // MARK: - Setup
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        // Do any additional setup after loading the view.

        photoHelper.delegate = self
        
        // Fail-safe.
        if pub == nil {
            dismiss(animated: true, completion: nil)
        } else {
            
            weak var weakSelf = self
            TCPAPIHelper.APICalls.PubAPI.getPub(pub: pub!) { (success: Bool, thePub: Pub?) in
                
                if success == false || thePub == nil {
                    
                    // Show error alert.
                    let alert = UIAlertController(title: Strings.Alerts.Titles.error.capitalized, message: Strings.Alerts.Messages.searchFailed, preferredStyle: .alert)
                    let ok = UIAlertAction(title: Strings.General.ok.capitalized, style: .cancel, handler: nil)
                    alert.addAction(ok)
                    weakSelf!.present(alert, animated: true, completion: nil)
                    
                } else {
                    
                    weakSelf!.pub = thePub!
                                        
                    // Set the pub details in the UI.
                    
                    if weakSelf!.pub!.pubName != nil {
                        weakSelf!.nameLabel.text = weakSelf!.pub!.pubName!
                    }
                    
                    if weakSelf!.pub!.pubAddress != nil {
                        weakSelf!.addrLabel.text = weakSelf!.pub!.pubAddress!
                    }
                    
                    weakSelf!.pubTimes = Pub.getPubTimes(pub: weakSelf!.pub!)
                    weakSelf!.drinkTypes = Pub.getDrinkPrices(pub: weakSelf!.pub!)
                    weakSelf!.pubFeatures = Pub.getPubFeatures(pub:  weakSelf!.pub!)

                    // Setup the UI based on the pub data.
                    DispatchQueue.main.async {
                        weakSelf!.setupUI()
                    }
                }
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - UI
    
    func setupUI() {
        
        // Gesture recognizers.
        
         let contactTgr = UITapGestureRecognizer(target: self, action: #selector(contactPressed))
        contactCont.addGestureRecognizer(contactTgr)
        
        let favouriteTgr = UITapGestureRecognizer(target: self, action: #selector(favouritePressed))
        favouriteCont.addGestureRecognizer(favouriteTgr)
        
        let directionsTgr = UITapGestureRecognizer(target: self, action: #selector(directionsPressed))
        directionsCont.addGestureRecognizer(directionsTgr)
        
        // Times table.
        
        timesTable.isScrollEnabled = false
        var timesContHeight: CGFloat = 0.0
        if pubTimes != nil && pubTimes!.count > 0 {
            
            // Get dynamic height of times container depending on it's count.
            timesContHeight = timesCellHeight * CGFloat(pubTimes!.count + 1)
            for constraint in timesCont.constraints {
                if constraint.identifier == "height" {
                    constraint.constant = timesContHeight
                    timesTable.reloadData()
                }
            }
        }
        
        // Features table.
        
        featuresTable.isScrollEnabled = false
        var featuresContHeight: CGFloat = 0.0
        if pubFeatures != nil && pubFeatures!.count > 0 {
            
            // Get dynamic height of features container depending on it's count.
            featuresContHeight = featuresCellHeight * CGFloat(pubFeatures!.count + 1)
            for constraint in featuresCont.constraints {
                if constraint.identifier == "height" {
                    constraint.constant = featuresContHeight
                    featuresTable.reloadData()
                }
            }
        }
    }
    
    // MARK: - Buttons
    
    @IBAction func backBtnPressed(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func updateBtnPressed(_ sender: Any) {
    
        let destVC = storyboard?.instantiateViewController(withIdentifier: "UpdatePubVC") as? UpdatePubVC
        destVC!.pub = pub!
        present(destVC!, animated: true, completion: nil)
    }
    
    @IBAction func pictureBtnPressed(_ sender: Any) {
    
        photoHelper.chooseSource(vc: self)
    }
    
    @IBAction func drinkMenuBtnPressed(_ sender: Any) {
        
        if pub == nil || drinkTypes == nil {
        
            // Show the user an error.
            let alert = UIAlertController(title: Strings.Alerts.Titles.error.capitalized, message: Strings.Alerts.Messages.noDrinkPrices, preferredStyle: .alert)
            let ok = UIAlertAction(title: Strings.General.ok.capitalized, style: .cancel, handler: nil)
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
            
        } else {
            
            let destVC = storyboard?.instantiateViewController(withIdentifier: "DrinksMenuVC") as? DrinksMenuVC
            destVC!.pub = pub!
            destVC!.drinkTypes = drinkTypes!
            present(destVC!, animated: true, completion: nil)
        }
    }

    @IBAction func reviewsBtnPressed(_ sender: Any) {
    }
    
    @objc func contactPressed() {
        
    }
    
    @objc func favouritePressed() {
        
    }
    
    @objc func directionsPressed() {
        
        // Get the pub address and open the maps app with directions.
        
        if pub === nil {
            
            let alert = UIAlertController(title: Strings.Alerts.Titles.error.capitalized, message: Strings.Alerts.Messages.failedToDirect, preferredStyle: .alert)
            let ok = UIAlertAction(title: Strings.General.ok.capitalized, style: .cancel, handler: nil)
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
            
            
        } else {
            
            weak var weakSelf = self
            MapHelper.getDirections(pub: pub!) { (success: Bool) in
                
                if success == false {
                    
                    let alert = UIAlertController(title: Strings.Alerts.Titles.error.capitalized, message: Strings.Alerts.Messages.failedToDirect, preferredStyle: .alert)
                    let ok = UIAlertAction(title: Strings.General.ok.capitalized, style: .cancel, handler: nil)
                    alert.addAction(ok)
                    weakSelf!.present(alert, animated: true, completion: nil)
                }
            }
        }
    }
    
    // MARK: - Photo Helper Delegate
    
    func pictureChosen(img: UIImage?) {
        
        if img == nil {
            
            let alert = UIAlertController(title: Strings.Alerts.Titles.error.capitalized, message: Strings.Alerts.Messages.failedToPicture, preferredStyle: .alert)
            let ok = UIAlertAction(title: Strings.General.ok.capitalized, style: .cancel, handler: nil)
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
            
        } else {
            
            pubImgView.image = img
            TCPAPIHelper.APICalls.PubAPI.addPubPhoto(pubID: pub!.pubID!, pubImg: img!) { (success: Bool) in
                
                print("HERE!")
            }
        }
    }
    
    // MARK: - UI Table View Delegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == timesTable {
           
            // Times
            
            if pubTimes == nil || pubTimes!.count < 1 {
                return 0
            } else {
                return pubTimes!.count
            }
            
        } else {
            
            // Features.
            
            if pubFeatures == nil || pubFeatures!.count < 1 {
                return 0
            } else {
                return pubFeatures!.count
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if tableView == timesTable {
            return timesCellHeight
        } else {
            return featuresCellHeight
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cellIdentifier = "cellIdentifier"
        var cell: UITableViewCell? = nil
        if cell == nil {
            cell = UITableViewCell(style: .subtitle, reuseIdentifier: cellIdentifier)
        }
        cell?.selectionStyle = .none
        cell?.clipsToBounds = true
        
        if tableView == timesTable {

            // Times.
            
            if pubTimes != nil && indexPath.row < pubTimes!.count {
                
                let pubTime: PubTimes = pubTimes![indexPath.row]
                if pubTime.day != nil && pubTime.day != nil && pubTime.day != nil {
                    
                    cell!.textLabel!.text = pubTime.day!.capitalized
                    cell!.detailTextLabel!.text = pubTime.opening! + " - " + pubTime.closing!
                }
            }
            
        } else {
            
            // Features.
            
            if pubFeatures != nil && indexPath.row < pubFeatures!.count {
                cell!.textLabel!.text = pubFeatures![indexPath.row]
            }
        }
        
        if cell == nil {
            return UITableViewCell()
        } else {
            return cell!
        }
    }
}
