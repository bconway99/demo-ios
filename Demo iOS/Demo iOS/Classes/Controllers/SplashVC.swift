//
//  SplashVC.swift
//  Demo iOS
//
//  Created by Ben Conway on 20/03/2018.
//  Copyright © 2018 Ben Conway. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift

class SplashVC: UIViewController {

    // MARK: - Setup
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        checkDest()
    }
    
    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func checkDest() {
        
        // Wait 2 seconds before deciding which screen to push to.
        weak var weakSelf = self
        let delaySecs = DispatchTime.now() + 0.0//2.0
        DispatchQueue.main.asyncAfter(deadline: delaySecs) {
            
            let accessToken: String? = UserDefsHelper.Tokens.getAccessToken()
            // Check if the user is currently logged in or not and if the access token isn't nil.
            if UserDefsHelper.SignIn.isSignedIn() == true && accessToken != nil {
                
                // Then check if the access token has expired or can still be used in API calls.
                if JWTHelper.isExpired(token: accessToken!) == true {
                    
                    // The user's access token has expired so log them out and clear their data.
                    weak var weakSelf = self
                    UserDefsHelper.Tokens.clearAccessToken(completionHandler: { (success: Bool) in
                        
                        MagicalRecordHelper.clearData(completionHandler: { (success: Bool) in
                            
                            UserDefsHelper.SignIn.setSignIn(signedIn: false)
                            NetworkUtil.setNetworkActivityIndicator(visible: false)
                            // Send the user to the login view.
                            let destVC = weakSelf!.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as? LoginVC
                            weakSelf!.present(destVC!, animated: true, completion: nil)
                        })
                    })
                    
                } else {
                    
                    // Send the user to the map view, but implement the sliding view controller.
                    let mapVC = weakSelf!.storyboard?.instantiateViewController(withIdentifier: "MapVC") as? MapVC
                    let menuVC = weakSelf!.storyboard?.instantiateViewController(withIdentifier: "MenuVC") as? MenuVC
                    let destVC = SlideMenuController(mainViewController: mapVC!, leftMenuViewController: menuVC!)
                    weakSelf!.present(destVC, animated: true, completion: nil)
                }
                
            } else {
                
                // Send the user to the login view.
                let destVC = weakSelf!.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as? LoginVC
                weakSelf!.present(destVC!, animated: true, completion: nil)
            }
        }
    }
}
