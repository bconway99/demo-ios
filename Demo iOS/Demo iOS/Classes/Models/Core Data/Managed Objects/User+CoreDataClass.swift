//
//  User+CoreDataClass.swift
//  Demo iOS
//
//  Created by Ben Conway on 13/05/2018.
//  Copyright © 2018 Ben Conway. All rights reserved.
//
//

import Foundation
import CoreData

@objc(User)
public class User: NSManagedObject {
    
    static func checkIfAlreadyExists(userID: String?) -> AnyObject? {
        
        if userID == nil {
            return nil
        }
        let predicate = NSPredicate(format: "%K == %@", "userID", userID!)
        if let coreData = User.mr_findFirst(with: predicate) {
            return coreData
        }
        return nil
    }
    
    // Convert the JSON objectionary into a Core Data object.
    static func fromJSON(object: [String : Any]?, userType: String, context: NSManagedObjectContext) -> User? {
        
        // Fail-safe check.
        if object == nil {
            return nil
        }
        
        // Check a unique value from the passed in JSON (the ID) to check if an entity already exists with that ID.
        // If it does then just append that entity and if it doesn't then create a new entity and begin populating it's values.
        var coreData = self.checkIfAlreadyExists(userID: object![JSONKeys.userID] as? String) as! User?
        if coreData == nil {
            coreData = User(context: context)
        }
        
        // Fail-safe check.
        if coreData == nil {
            return nil
        }
        
        // Pass the values from the JSON object to the Core Data object, if they are not nil.
        
        if object![JSONKeys.emailVerified] != nil && object![JSONKeys.emailVerified] as? Bool != nil {
            coreData!.emailVerified = object![JSONKeys.emailVerified] as! Bool
        }
        
        if object![JSONKeys.email] != nil {
            coreData!.email = object![JSONKeys.email] as? String
        }
        
        if object![JSONKeys.name] != nil {
            coreData!.name = object![JSONKeys.name] as? String
        }
        
        if object![JSONKeys.picture] != nil {
            coreData!.picture = object![JSONKeys.picture] as? String
        }
        
        if object![JSONKeys.sub] != nil {
            coreData!.sub = object![JSONKeys.sub] as? String
        }
        
        if object![JSONKeys.userID] != nil {
            coreData!.userID = object![JSONKeys.userID] as? String
        }
        
        coreData!.userType = userType
        
        return coreData!
    }
    
    // Convert the Core Data object into a JSON objectionary.
    static func toJSON(coreData: User?) -> [String : Any]? {
        
        // Fail-safe check.
        if coreData == nil {
            return nil
        }
        
        // Create a new JSON objectionary.
        var object: [String : Any] = [:]
        
        // Pass the values from the Core Data object to the JSON object, if they are not nil.
        
        object[JSONKeys.emailVerified] = coreData!.emailVerified
        
        if coreData!.email != nil {
            object[JSONKeys.email] = coreData!.email!
        }
        
        if coreData!.name != nil {
            object[JSONKeys.name] = coreData!.name!
        }
        
        if coreData!.picture != nil {
            object[JSONKeys.picture] = coreData!.picture!
        }
        
        if coreData!.sub != nil {
            object[JSONKeys.sub] = coreData!.sub!
        }
        
        if coreData!.userID != nil {
            object[JSONKeys.userID] = coreData!.userID!
        }
        
        if coreData!.userType != nil {
            object[JSONKeys.userType] = coreData!.userType!
        }
        
        // Fail-safe check in case we couldn't add any of the Core Data values to the JSON object.
        if object.count < 1 {
            return nil
        } else {
            return object
        }
    }
}
