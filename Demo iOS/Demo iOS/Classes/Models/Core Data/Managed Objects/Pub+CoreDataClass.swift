//
//  Pub+CoreDataClass.swift
//  Demo iOS
//
//  Created by Ben Conway on 04/04/2018.
//  Copyright © 2018 Ben Conway. All rights reserved.
//
//

import Foundation
import CoreData

@objc(Pub)
public class Pub: NSManagedObject {
    
    static func checkIfAlreadyExists(pubID: String?) -> AnyObject? {
        
        if pubID == nil {
            return nil
        }
        let predicate = NSPredicate(format: "%K == %@", "pubID", pubID!)
        if let coreData = Pub.mr_findFirst(with: predicate) {
            return coreData
        }
        return nil
    }
    
    // Convert the JSON objectionary into a Core Data object.
    static func fromJSON(object: [String : Any]?, context: NSManagedObjectContext) -> Pub? {
        
        // Fail-safe check.
        if object == nil {
            return nil
        }
        
        // Check a unique value from the passed in JSON (the ID) to check if an entity already exists with that ID.
        // If it does then just append that entity and if it doesn't then create a new entity and begin populating it's values.
        var coreData = self.checkIfAlreadyExists(pubID: object![JSONKeys.pubID] as? String) as! Pub?
        if coreData == nil {
            coreData = Pub(context: context)
        }
        
        // Fail-safe check.
        if coreData == nil {
            return nil
        }
        
        // Pass the values from the JSON object to the Core Data object, if they are not nil.
        
        if object![JSONKeys.pubAddress] != nil {
            coreData!.pubAddress = object![JSONKeys.pubAddress] as? String
        }
        
        if object![JSONKeys.pubLatitude] != nil && object![JSONKeys.pubLatitude] as? Double != nil {
            coreData!.pubLatitude = object![JSONKeys.pubLatitude] as! Double
        }
        
        if object![JSONKeys.pubLongitude] != nil && object![JSONKeys.pubLongitude] as? Double != nil {
            coreData!.pubLongitude = object![JSONKeys.pubLongitude] as! Double
        }
        
        if object![JSONKeys.pubDistance] != nil && object![JSONKeys.pubDistance] as? Double != nil {
            coreData!.pubDistance = object![JSONKeys.pubDistance] as! Double
        }
        
        if object![JSONKeys.pubFranchise] != nil {
            coreData!.pubFranchise = object![JSONKeys.pubFranchise] as? String
        }
        
        if object![JSONKeys.pubID] != nil {
            coreData!.pubID = object![JSONKeys.pubID] as? String
        }
        
        if object![JSONKeys.pubName] != nil {
            coreData!.pubName = object![JSONKeys.pubName] as? String
        }
        
        if object![JSONKeys.pubTime] != nil {
            coreData!.pubTime = object![JSONKeys.pubTime] as? String
        }
        
        if object![JSONKeys.pubWebsite] != nil {
            coreData!.pubWebsite = object![JSONKeys.pubWebsite] as? String
        }
        
        if object![JSONKeys.pubTimes] != nil {
            coreData!.pubTimes = object![JSONKeys.pubTimes] as? NSObject
        }
        
        if object![JSONKeys.drinkPrices] != nil {
            coreData!.drinkPrices = object![JSONKeys.drinkPrices] as? NSObject
        }
        
        if object![JSONKeys.pubFeatures] != nil {
            coreData!.pubFeatures = object![JSONKeys.pubFeatures] as? NSObject
        }
        
        if object![JSONKeys.pubCity] != nil {
            coreData!.pubCity = object![JSONKeys.pubCity] as? String
        }
        
        if object![JSONKeys.pubCountry] != nil {
            coreData!.pubCountry = object![JSONKeys.pubCountry] as? String
        }
        
        return coreData!
    }
    
    // Convert the Core Data object into a JSON objectionary.
    static func toJSON(coreData: Pub?) -> [String : Any]? {
        
        // Fail-safe check.
        if coreData == nil {
            return nil
        }
        
        // Create a new JSON objectionary.
        var object: [String : Any] = [:]
        
        // Pass the values from the Core Data object to the JSON object, if they are not nil.
        
        if coreData!.pubAddress != nil {
            object[JSONKeys.pubAddress] = coreData!.pubAddress!
        }
        
        object[JSONKeys.pubLatitude] = coreData!.pubLatitude
        
        object[JSONKeys.pubLongitude] = coreData!.pubLongitude
        
        object[JSONKeys.pubDistance] = coreData!.pubDistance
        
        if coreData!.pubFranchise != nil {
            object[JSONKeys.pubFranchise] = coreData!.pubFranchise!
        }
        
        if coreData!.pubID != nil {
            object[JSONKeys.pubID] = coreData!.pubID!
        }
        
        if coreData!.pubName != nil {
            object[JSONKeys.pubName] = coreData!.pubName!
        }
        
        if coreData!.pubTime != nil {
            object[JSONKeys.pubTime] = coreData!.pubTime!
        }
        
        if coreData!.pubWebsite != nil {
            object[JSONKeys.pubWebsite] = coreData!.pubWebsite!
        }
        
        if coreData!.pubTimes != nil {
            object[JSONKeys.pubTimes] = coreData!.pubTimes!
        }
        
        if coreData!.drinkPrices != nil {
            object[JSONKeys.drinkPrices] = coreData!.drinkPrices!
        }
        
        if coreData!.pubFeatures != nil {
            object[JSONKeys.pubFeatures] = coreData!.pubFeatures!
        }
        
        if coreData!.pubCity != nil {
            object[JSONKeys.pubCity] = coreData!.pubCity!
        }
        
        if coreData!.pubCountry != nil {
            object[JSONKeys.pubCountry] = coreData!.pubCountry!
        }
        
        // Fail-safe check in case we couldn't add any of the Core Data values to the JSON object.
        if object.count < 1 {
            return nil
        } else {
            return object
        }
    }
    
    // Get the coordinate values from the coordinates object.
    static func getCoordinates(pub: Pub) -> [Double]? {
        
        return [pub.pubLongitude, pub.pubLatitude]
    }
    
    // Convert the pub JSON to a string.
    static func toJSONStr(object: [String : Any]) -> String? {
        
        do {
            
            let data = try JSONSerialization.data(withJSONObject: object, options: .prettyPrinted)
            let str = String(data: data, encoding: .utf8)
            if str == nil {
                return nil
            } else {
                return str!
            }
            
        } catch let error {
            
            print("Pub search request to string error: \(error.localizedDescription)")
            return nil
        }
    }
    
    // Extract the pub times from the pub object.
    static func getPubTimes(pub: Pub) -> [PubTimes]? {
        
        // Fail-safe.
        guard let times = pub.pubTimes as? [String : [String : AnyObject]] else {
            return nil
        }
        
        var pubTimesArray: [PubTimes] = []
        
        // Iterate over the pub times and create an array.
        for time in times {
            
            let closing = time.value[JSONKeys.closing]! as? String
            let opening = time.value[JSONKeys.opening]! as? String
            if closing != nil && opening != nil {
                
                let pubTimes = PubTimes(day: time.key, opening: opening!, closing: closing!)
                pubTimesArray.append(pubTimes)
            }
        }
        
        // Refactor the dates in the correct order.
        for pubTime in pubTimesArray {
            
            // Fail-safe.
            if pubTime.day == nil {
                continue
            }
            
            switch pubTime.day {
                
            case JSONKeys.PubDays.monday:
                pubTimesArray[0] = pubTime
                break
                
            case JSONKeys.PubDays.tuesday:
                pubTimesArray[1] = pubTime
                break
                
            case JSONKeys.PubDays.wednesday:
                pubTimesArray[2] = pubTime
                break
                
            case JSONKeys.PubDays.thursday:
                pubTimesArray[3] = pubTime
                break
                
            case JSONKeys.PubDays.friday:
                pubTimesArray[4] = pubTime
                break
                
            case JSONKeys.PubDays.saturday:
                pubTimesArray[5] = pubTime
                break
                
            case JSONKeys.PubDays.sunday:
                pubTimesArray[6] = pubTime
                break
                
            default:
                break
            }
        }
        
        if pubTimesArray.count < 1 {
            return nil
        } else {
            return pubTimesArray
        }
    }
    
    // Extract the pub features from the pub object.
    static func getPubFeatures(pub: Pub) -> [String]? {
        
        // Fail-safe.
        guard let features = pub.pubFeatures as? [String] else {
            return nil
        }
        
        var pubFeatureArray: [String] = []
        
        // Iterate over the pub features and create an array.
        // Then format each value from it's JSON key to a readable string.
        for feature in features {
            
            var formatted: String = feature
            formatted = formatted.replacingOccurrences(of: "_", with: " ")
            formatted = formatted.replacingOccurrences(of: "tv", with: "TV")
            formatted = formatted.replacingOccurrences(of: "wifi", with: "WiFi")
            pubFeatureArray.append(formatted.capitalized)
        }
        
        if pubFeatureArray.count < 1 {
            return nil
        } else {
            return pubFeatureArray
        }
    }
    
    // Extract the pub drinks from the pub object.
    static func getDrinkPrices(pub: Pub) -> [PubDrinkType]? {
        
        // Fail-safe.
        guard let drinkPrices = pub.drinkPrices as? [[String : AnyObject]] else {
            return nil
        }
        
        var pubDrinkTypes: [PubDrinkType] = []
        
        // Iterate over the pub drink types and create an array.
        for drinkPrice in drinkPrices {
            
            guard let drinkType = drinkPrice[JSONKeys.drinkType] as? String else {
                continue
            }
            
            guard let drinks = drinkPrice[JSONKeys.drinks] as? [[String : AnyObject]] else {
                continue
            }
            
            var pubDrinks: [PubDrink] = []
            var pubPrices: [PubPrice] = []
            // Iterate over all of the drinks in the current drink type.
            for drink in drinks {
                
                // Get the prices from the current drink.
                guard let prices = drink[JSONKeys.prices] as? [[String : AnyObject]] else {
                    continue
                }
                
                // Iterate over all of the prices in the current drink.
                for price in prices {
                    
                    // Fail-safe.
                    if price[JSONKeys.measurement] as? String == nil || price[JSONKeys.price] as? Double == nil {
                        continue
                    }
                    
                    let pubPrice = PubPrice(
                        
                        measurement: price[JSONKeys.measurement] as? String,
                        price: price[JSONKeys.price] as? Double
                    )
                    
                    // TODO: Duplicate drink prices are being added, check what's wrong with the parsing and remove this fail-safe.
                    // A fail-safe to check that this measurement hasn't already been added to the pub price.
                    // This solves a bug of having duplicate measurement in the table.
                    if pubPrice.measurement != nil {
                        if pubPrices.contains(where: { $0.measurement == pubPrice.measurement! }) == false {
                            pubPrices.append(pubPrice)
                        }
                    }
                }
                
                // Fail-safe.
                if pubPrices.count < 1 {
                    continue
                }
                
                let pubDrink = PubDrink(
                    drinkID: drink[JSONKeys.drinkID] as? String,
                    name: drink[JSONKeys.name] as? String,
                    menuCategory: drink[JSONKeys.menuCategory] as? String,
                    menuSubcategory: drink[JSONKeys.menuSubcategory] as? String,
                    prices: pubPrices
                )
                
                pubDrinks.append(pubDrink)
            }
            
            // Fail-safe.
            if pubDrinks.count < 1 {
                continue
            }
            
            let pubDrinkType = PubDrinkType(drinkType: drinkType, pubDrinks: pubDrinks)
            pubDrinkTypes.append(pubDrinkType)
        }
        
        if pubDrinkTypes.count < 1 {
            return nil
        } else {
            return pubDrinkTypes
        }
    }
}
