//
//  User+CoreDataProperties.swift
//  Demo iOS
//
//  Created by Ben Conway on 13/05/2018.
//  Copyright © 2018 Ben Conway. All rights reserved.
//
//

import Foundation
import CoreData


extension User {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<User> {
        return NSFetchRequest<User>(entityName: "User")
    }

    @NSManaged public var picture: String?
    @NSManaged public var name: String?
    @NSManaged public var email: String?
    @NSManaged public var sub: String?
    @NSManaged public var userID: String?
    @NSManaged public var emailVerified: Bool
    @NSManaged public var userType: String?

}
