//
//  Pub+CoreDataProperties.swift
//  Demo iOS
//
//  Created by Ben Conway on 23/07/2018.
//  Copyright © 2018 Ben Conway. All rights reserved.
//
//

import Foundation
import CoreData


extension Pub {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Pub> {
        return NSFetchRequest<Pub>(entityName: "Pub")
    }

    @NSManaged public var drinkPrices: NSObject?
    @NSManaged public var pubAddress: String?
    @NSManaged public var pubDistance: Double
    @NSManaged public var pubFeatures: NSObject?
    @NSManaged public var pubFranchise: String?
    @NSManaged public var pubID: String?
    @NSManaged public var pubLatitude: Double
    @NSManaged public var pubLongitude: Double
    @NSManaged public var pubName: String?
    @NSManaged public var pubTime: String?
    @NSManaged public var pubTimes: NSObject?
    @NSManaged public var pubWebsite: String?
    @NSManaged public var pubCountry: String?
    @NSManaged public var pubCity: String?

}
