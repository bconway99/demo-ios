//
//  DeploymentEnvironment.swift
//  Demo iOS
//
//  Created by Ben Conway on 12/03/2018.
//  Copyright © 2018 Ben Conway. All rights reserved.
//

import Foundation

enum DeploymentEnvironment {
    
    case Development // Main development environment.
    case QAT // Quality Assurance Testing environment.
    case UAT // User Acceptance Testing environment.
    case Production // Live production environment.
}
