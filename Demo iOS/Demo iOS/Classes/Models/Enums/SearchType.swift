//
//  SearchType.swift
//  Demo iOS
//
//  Created by Ben Conway on 29/04/2018.
//  Copyright © 2018 Ben Conway. All rights reserved.
//

import Foundation

enum SearchType {
    
    case CheapestPint
    case CheapestDrink
    case CheapestType
    case ClosestPub
}
