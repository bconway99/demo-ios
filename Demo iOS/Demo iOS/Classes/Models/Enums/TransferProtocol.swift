//
//  TransferProtocol.swift
//  Demo iOS
//
//  Created by Ben Conway on 12/03/2018.
//  Copyright © 2018 Ben Conway. All rights reserved.
//

import Foundation

enum TransferProtocol {
    
    case http
    case https
}
