//
//  MapPub.swift
//  Demo iOS
//
//  Created by Ben Conway on 14/07/2018.
//  Copyright © 2018 Ben Conway. All rights reserved.
//

import UIKit
import GoogleMaps

// A struct containing information about each pub on the map.
// Including both the pub data and it's map marker.

struct MapPub {
    
    var pub: Pub?
    var marker: GMSMarker?
}
