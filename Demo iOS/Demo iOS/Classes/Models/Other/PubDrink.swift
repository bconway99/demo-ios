//
//  PubDrink.swift
//  Demo iOS
//
//  Created by Ben Conway on 17/06/2018.
//  Copyright © 2018 Ben Conway. All rights reserved.
//

import Foundation

struct PubDrinkType {
    
    var drinkType: String?
    var pubDrinks: [PubDrink]?
}

struct PubDrink {
    
    var drinkID: String?
    var name: String?
    var menuCategory: String?
    var menuSubcategory: String?
    var prices: [PubPrice]?
}

struct PubPrice {
    
    var measurement: String?
    var price: Double?
}
