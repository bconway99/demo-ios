//
//  PubTimes.swift
//  Demo iOS
//
//  Created by Ben Conway on 13/06/2018.
//  Copyright © 2018 Ben Conway. All rights reserved.
//

import Foundation

struct PubTimes {
    
    var day: String?
    var opening: String?
    var closing: String?
}
