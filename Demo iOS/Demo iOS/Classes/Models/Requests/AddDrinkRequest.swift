//
//  AddDrinkRequest.swift
//  Demo iOS
//
//  Created by Ben Conway on 11/08/2018.
//  Copyright © 2018 Ben Conway. All rights reserved.
//

import Foundation
import Alamofire

class AddDrinkRequest: NSObject {

    var pubID: String?
    var drinkID: String?
    var drinkName: String?
    var category: String?
    var prices: [PubPrice]?
    
    // Convert the request into a JSON string.
    func toJSONStr(request: AddDrinkRequest?) -> String? {
    
        // Fail-safe.
        if
    
            pubID == nil ||
            drinkID == nil ||
            drinkName == nil ||
            category == nil ||
            prices == nil ||
            prices!.count < 1
    
        {
    
            return nil
    
        } else {
    
            var object: [String : Any] = [:]
            object[JSONKeys.pubID] = pubID!
            object[JSONKeys.drinkID] = drinkID!
            object[JSONKeys.drinkName] = drinkName!
            object[JSONKeys.category] = category!
            
            // Rather than an array of custom objects, we must make an array that can be converted to JSON.
            var pricesJSON: [[String : AnyObject]] = []
            for price in prices! {
                
                if price.measurement != nil && price.price != nil {
                   
                    let priceDict: [String : AnyObject] = [
                        JSONKeys.measurement : price.measurement! as AnyObject,
                        JSONKeys.price : price.price! as AnyObject
                    ]
                    pricesJSON.append(priceDict)
                }
            }
            
            // Fail-safe.
            if pricesJSON.count > 0 {
                object[JSONKeys.prices] = pricesJSON
            }
            
            do {
    
                let data = try JSONSerialization.data(withJSONObject: object, options: .prettyPrinted)
                let str = String(data: data, encoding: .utf8)
                if str == nil {
                    return nil
                } else {
                    return str!
                }
    
            } catch let error {
    
                print("Add pub request to string error: \(error.localizedDescription)")
                return nil
            }
        }
    }
    
    // Convert this object to an Alamofire Parameters object for the search API call.
    func toParams() -> Parameters {
        
        var theParams: Parameters = [:]
        
        if pubID != nil {
            theParams[JSONKeys.pubID] = pubID!
        }
        
        if drinkID != nil {
            theParams[JSONKeys.drinkID] = drinkID!
        }
        
        if drinkName != nil {
            theParams[JSONKeys.drinkName] = drinkName!
        }
        
        if category != nil {
            theParams[JSONKeys.category] = category!
        }
        
        if prices != nil {
            
            // Iterate over the prices array and unwrap all of the optionals.
            // As we cannot send up optional values in the JSON parameters.
            var unwrappedPrices = [[:]]
            for price in prices! {
                
                if
                    price.measurement != nil && price.measurement!.count > 0 &&
                    price.price != nil && price.price! >= 0.0
                {
                    unwrappedPrices.append([
                        JSONKeys.measurement : price.measurement!,
                        JSONKeys.price : price.price!
                    ])
                }
            }
            theParams[JSONKeys.prices] = unwrappedPrices
        }
        
        return theParams
    }
}
