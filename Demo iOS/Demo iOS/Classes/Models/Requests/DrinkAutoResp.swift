//
//  DrinkAutoResp.swift
//  Demo iOS
//
//  Created by Ben Conway on 23/04/2018.
//  Copyright © 2018 Ben Conway. All rights reserved.
//

import Foundation

struct DrinkAutoResp {
    
    var drinkName: String?
    var drinkID: String?
    var drinkType: String?
    var drinkParent: String?
}
