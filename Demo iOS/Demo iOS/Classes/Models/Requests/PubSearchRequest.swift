//
//  Pubswift
//  Demo iOS
//
//  Created by Ben Conway on 25/03/2018.
//  Copyright © 2018 Ben Conway. All rights reserved.
//

import Foundation
import Alamofire

class PubSearchRequest: NSObject {

    var distance: Int?
    var distanceUnits: String?
    var drink: String?
    var drinkType: String?
    var options: [String]?
    var searchType: SearchType?
    var userLocation: [String : Double]?
    
    // Convert the request into a JSON string.
    func toJSONStr(request: PubSearchRequest?) -> String? {
        
        // Fail-safe.
        if searchType == nil {
            return nil
        }
        
        var object: [String : Any] = [:]
        
        if distance != nil {
            object[JSONKeys.distance] = distance!
        }
        
        if distanceUnits != nil {
            object[JSONKeys.distanceUnits] = distanceUnits!
        }

        // For the search types CheapestPint, CheapestType or ClosestPub.
        // Only send up the drink type and not the drink name.
        // So don't add the drink name to the JSON string.
        if searchType! != .CheapestPint && searchType! != .CheapestType && searchType! != .ClosestPub {

            if drink != nil {
                object[JSONKeys.drink] = drink!
            }
        }
        
        // For the search type ClosestPub.
        // Don't send up the drink type or the drink name.
        // So don't add either to the JSON string.
        if searchType! != .ClosestPub {
            
            if drinkType != nil {
                object[JSONKeys.drinkType] = drinkType!
            }
        }
        
        if options != nil {
            object[JSONKeys.options] = options!
        }
        
        if searchType != nil {
            object[JSONKeys.searchType] = getSearchTypeStr()
        }
        
        if userLocation != nil {
            object[JSONKeys.userLocation] = userLocation!
        }
        
        do {
            
            let data = try JSONSerialization.data(withJSONObject: object, options: .prettyPrinted)
            let str = String(data: data, encoding: .utf8)
            if str == nil {
                return nil
            } else {
                return str!
            }
        
        } catch let error {
            
            print("Pub search request to string error: \(error.localizedDescription)")
            return nil
        }
    }
    
    // Convert this object to an Alamofire Parameters object for the search API call.
    func toParams() -> Parameters {
        
        var theParams: Parameters = [:]

        if distance != nil {
            theParams[JSONKeys.distance] = distance!
        }
        
        if distanceUnits != nil {
            theParams[JSONKeys.distanceUnits] = distanceUnits!
        }
        
        if drink != nil {
            theParams[JSONKeys.drink] = drink!
        }
        
        if drinkType != nil {
            theParams[JSONKeys.drinkType] = drinkType!
        }
        
        if options != nil {
            theParams[JSONKeys.options] = options!
        }
        
        if searchType != nil {
            theParams[JSONKeys.searchType] = getSearchTypeStr()
        }
        
        if userLocation != nil {
            theParams[JSONKeys.userLocation] = userLocation!
        }
        
        return theParams
    }
    
    // Create the user location dictionary.
    func addUserLocation(latitude: Double, longitude: Double) {
        
        userLocation = [
        
            JSONKeys.latitude : latitude,
            JSONKeys.longitude : longitude
        ]
    }
    
    // Add or remove an option with a key.
    func setOption(key: String, on: Bool) {
        
        // Create an array if we haven't already.
        if options == nil {
            options = []
        }
        
        if options != nil {
            
            if on == true {
            
                // Add the key to the array.
                options!.append(key)
            
            } else {
                
                // Remove the key from the array.
                if let idx = options!.index(of: key) {
                    options!.remove(at: idx)
                }
            }
        }
        
        // If the user removes all of the options then set the array as nil.
        // So we don't add it to the JSON in the API call.
        if options != nil && options!.count < 1 {
            options = nil
        }
    }
    
    // Setting the search type and edit any other values if needs be.
    func setSearchType(str: String) {
        
        switch str {
            
        case Strings.SearchTypes.cheapestPint:
            
            // Should just be type (Ale, Lager).
            searchType = .CheapestPint
            break
            
        case Strings.SearchTypes.cheapestDrink:
            
            // Should send up the drink type (Ale, Lager) and the drink name.
            searchType = .CheapestDrink
            
            break
            
        case Strings.SearchTypes.cheapestType:
            
            // Should just be type (Ale, Lager).
            searchType = .CheapestType
            
            break
            
        default:
            
            // Can send anything up.
            searchType = .ClosestPub
            
            break
        }
    }
    
    // Get the search type string depending on the search type.
    func getSearchTypeStr() -> String {
        
        if searchType == nil {
        
            // Fail-safe.
            return JSONKeys.SearchTypes.closestPub

        } else {
            
            switch searchType! {
                
            case .CheapestPint:
                return JSONKeys.SearchTypes.cheapestPint
                
            case .CheapestDrink:
                return JSONKeys.SearchTypes.cheapestDrink
                
            case .CheapestType:
                return JSONKeys.SearchTypes.cheapestType
                
            case .ClosestPub:
                return JSONKeys.SearchTypes.closestPub
            }
        }
    }
}
