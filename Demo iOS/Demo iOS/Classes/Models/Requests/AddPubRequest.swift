//
//  AddPubRequest.swift
//  Demo iOS
//
//  Created by Ben Conway on 17/05/2018.
//  Copyright © 2018 Ben Conway. All rights reserved.
//

import Foundation
import Alamofire

class AddPubRequest: NSObject {

    var pubName: String?
    var pubAddress: String?
    var pubFranchise: String?
    var pubTimes: [String : [String : String]]?
    var pubFeatures: [String]?
    var pubCity: String?
    var pubCountry: String?
    var pubWebsite: String?
    var pubLatitude: Double?
    var pubLongitude: Double?
    
    // Convert the request into a JSON string.
    func toJSONStr(request: AddPubRequest?) -> String? {
        
        // Fail-safe.
        if
            
            pubName == nil ||
            pubAddress == nil ||
            pubFranchise == nil ||
            pubTimes == nil ||
            pubFeatures == nil ||
            pubCity == nil ||
            pubCountry == nil ||
            pubWebsite == nil ||
            pubLatitude == nil ||
            pubLongitude == nil
        
        {
        
            return nil
        
        } else {
            
            var object: [String : Any] = [:]
            object[JSONKeys.pubName] = pubName!
            object[JSONKeys.pubAddress] = pubAddress!
            object[JSONKeys.pubFranchise] = pubFranchise!
            object[JSONKeys.pubTimes] = pubTimes!
            object[JSONKeys.pubFeatures] = pubFeatures!
            object[JSONKeys.pubCity] = pubCity!
            object[JSONKeys.pubCountry] = pubCountry!
            object[JSONKeys.pubWebsite] = pubWebsite!
            object[JSONKeys.pubLatitude] = pubLatitude!
            object[JSONKeys.pubLongitude] = pubLongitude!

            do {
                
                let data = try JSONSerialization.data(withJSONObject: object, options: .prettyPrinted)
                let str = String(data: data, encoding: .utf8)
                if str == nil {
                    return nil
                } else {
                    return str!
                }
                
            } catch let error {
                
                print("Add pub request to string error: \(error.localizedDescription)")
                return nil
            }
        }
    }
    
    // Convert this object to an Alamofire Parameters object for the search API call.
    func toParams() -> Parameters {
        
        var theParams: Parameters = [:]
        
        if pubName != nil {
            theParams[JSONKeys.pubName] = pubName!
        }
        
        if pubAddress != nil {
            theParams[JSONKeys.pubAddress] = pubAddress!
        }
        
        if pubFranchise != nil {
            theParams[JSONKeys.pubFranchise] = pubFranchise!
        }
        
        if pubTimes != nil {
            theParams[JSONKeys.pubTimes] = pubTimes!
        }
        
        if pubFeatures != nil {
            theParams[JSONKeys.pubFeatures] = pubFeatures!
        }
        
        if pubCity != nil {
            theParams[JSONKeys.pubCity] = pubCity!
        }
        
        if pubCountry != nil {
            theParams[JSONKeys.pubCountry] = pubCountry!
        }
        
        if pubWebsite != nil {
            theParams[JSONKeys.pubWebsite] = pubWebsite!
        }
        
        if pubLatitude != nil {
            theParams[JSONKeys.pubLatitude] = pubLatitude!
        }
        
        if pubLongitude != nil {
            theParams[JSONKeys.pubLongitude] = pubLongitude!
        }
        
        return theParams
    }
    
    // Add the opening and closing times for each day.
    func addTimes() -> [String : [String : String]]? {
        
        var times: [String : [String : String]] = [:]
        
        // TODO: When the UI is complete allow the user to add each day manually.
        for i in 0..<7 {
            
            switch i {
                
            case 0:
                times["monday"] = ["opening" : "08:00", "closing" : "12:00"]
                break
                
            case 1:
                times["tuesday"] = ["opening" : "08:00", "closing" : "12:00"]
                break
                
            case 2:
                times["wednesday"] = ["opening" : "08:00", "closing" : "12:00"]
                break
                
            case 3:
                times["thursday"] = ["opening" : "08:00", "closing" : "12:00"]
                break
                
            case 4:
                times["friday"] = ["opening" : "08:00", "closing" : "12:00"]
                break
                
            case 5:
                times["saturday"] = ["opening" : "08:00", "closing" : "12:00"]
                break
                
            case 6:
                times["sunday"] = ["opening" : "08:00", "closing" : "12:00"]
                break
                
            default:
                break
            }            
        }
        
        if times.count < 1 {
            return nil
        } else {
            return times
        }
    }
}
