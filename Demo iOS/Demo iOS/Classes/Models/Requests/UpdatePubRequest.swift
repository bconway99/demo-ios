//
//  UpdatePubRequest.swift
//  Demo iOS
//
//  Created by Ben Conway on 20/05/2018.
//  Copyright © 2018 Ben Conway. All rights reserved.
//

import Foundation
import Alamofire

class UpdatePubRequest: NSObject {
    
    var pubID: String?
    var pubName: String?
    var pubAddress: String?
    var pubFranchise: String?
    var pubTimes: [String : [String : String]]?
    var pubFeatures: [String]?
    var pubCity: String?
    var pubCountry: String?
    var pubWebsite: String?
    var pubLatitude: Double?
    var pubLongitude: Double?
    
    // Convert the request into a JSON string.
    func toJSONStr(request: UpdatePubRequest?) -> String? {
        
        // Fail-safe.
        if pubID == nil {
            
            return nil
            
        } else {
            
            var object: [String : Any] = [:]
            object[JSONKeys.pubID] = pubID!

            // If any value is not nil, then the user has set it to be updated on the server.

            if pubName != nil {
                object[JSONKeys.pubName] = pubName!
            }
            
            if pubAddress != nil {
                object[JSONKeys.pubAddress] = pubAddress!
            }
            
            if pubFranchise != nil {
                object[JSONKeys.pubFranchise] = pubFranchise!
            }
            
            if pubTimes != nil {
                object[JSONKeys.pubTimes] = pubTimes!
            }
            
            if pubFeatures != nil {
                object[JSONKeys.pubFeatures] = pubFeatures!
            }
            
            if pubCity != nil {
                object[JSONKeys.pubCity] = pubCity!
            }
            
            if pubCountry != nil {
                object[JSONKeys.pubCountry] = pubCountry!
            }
            
            if pubWebsite != nil {
                object[JSONKeys.pubWebsite] = pubWebsite!
            }
            
            if pubLatitude != nil {
                object[JSONKeys.pubLatitude] = pubLatitude!
            }
            
            if pubLongitude != nil {
                object[JSONKeys.pubLongitude] = pubLongitude!
            }
            
            do {
                
                let data = try JSONSerialization.data(withJSONObject: object, options: .prettyPrinted)
                let str = String(data: data, encoding: .utf8)
                if str == nil {
                    return nil
                } else {
                    return str!
                }
                
            } catch let error {
                
                print("Add pub request to string error: \(error.localizedDescription)")
                return nil
            }
        }
    }
    
    // Convert this object to an Alamofire Parameters object for the search API call.
    func toParams() -> Parameters {
        
        var theParams: Parameters = [:]
        
        if pubID != nil {
            theParams[JSONKeys.pubID] = pubID!
        }
        
        if pubName != nil {
            theParams[JSONKeys.pubName] = pubName!
        }
        
        if pubAddress != nil {
            theParams[JSONKeys.pubAddress] = pubAddress!
        }
        
        if pubFranchise != nil {
            theParams[JSONKeys.pubFranchise] = pubFranchise!
        }
        
        if pubTimes != nil {
            theParams[JSONKeys.pubTimes] = pubTimes!
        }
        
        if pubFeatures != nil {
            theParams[JSONKeys.pubFeatures] = pubFeatures!
        }
        
        if pubCity != nil {
            theParams[JSONKeys.pubCity] = pubCity!
        }
        
        if pubCountry != nil {
            theParams[JSONKeys.pubCountry] = pubCountry!
        }
        
        if pubWebsite != nil {
            theParams[JSONKeys.pubWebsite] = pubWebsite!
        }
        
        if pubLatitude != nil {
            theParams[JSONKeys.pubLatitude] = pubLatitude!
        }
        
        if pubLongitude != nil {
            theParams[JSONKeys.pubLongitude] = pubLongitude!
        }

        return theParams
    }
    
    // The UpdatePub request requires all of the pub's current values, not just the ones being updated.
    func setCurValues(pub: Pub, completion: @escaping () -> Void) {
        
        if pub.pubID != nil {
            pubID = pub.pubID!
        }
        
        if pub.pubName != nil {
            pubName = pub.pubName!
        }
        
        if pub.pubAddress != nil {
            pubAddress = pub.pubAddress!
        }
        
        if pub.pubFranchise != nil {
            pubFranchise = pub.pubFranchise!
        }
        
        if pub.pubTimes != nil && pub.pubTimes! as? [String : [String : String]] != nil {
            pubTimes = pub.pubTimes! as? [String : [String : String]]
        }
        
        if pub.pubFeatures != nil && pub.pubFeatures! as? [String] != nil {
            pubFeatures = pub.pubFeatures! as? [String]
        }
        
        if pub.pubCity != nil {
            pubCity = pub.pubCity!
        }
        
        if pub.pubCountry != nil {
            pubCountry = pub.pubCountry!
        }
        
        if pub.pubWebsite != nil {
            pubWebsite = pub.pubWebsite!
        }
        
        pubLatitude = pub.pubLatitude
    
        pubLongitude = pub.pubLongitude
        
        completion()
    }
}
