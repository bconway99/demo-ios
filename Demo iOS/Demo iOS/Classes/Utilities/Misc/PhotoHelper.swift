//
//  PhotoHelper.swift
//  Demo iOS
//
//  Created by Ben Conway on 05/08/2018.
//  Copyright © 2018 Ben Conway. All rights reserved.
//

import UIKit

protocol PhotoHelperDelegate: class {
    
    func pictureChosen(img: UIImage?)
}

class PhotoHelper: NSObject, UIImagePickerControllerDelegate, UIPopoverControllerDelegate, UINavigationControllerDelegate {
    
    weak var delegate: PhotoHelperDelegate?
    var picker: UIImagePickerController? = UIImagePickerController()

    // MARK: - Get Picture
    
    func chooseSource(vc: UIViewController) {
        
        weak var weakSelf = self
        let alert = UIAlertController(title: Strings.Alerts.Titles.chooseSource, message: nil, preferredStyle: .alert)
        let camera = UIAlertAction(title: Strings.General.camera.capitalized, style: .default) { (action: UIAlertAction) in
            weakSelf!.openCamera(vc: vc)
        }
        let gallery = UIAlertAction(title: Strings.General.photoLibrary.capitalized, style: .default) { (action: UIAlertAction) in
            weakSelf!.openPhotoLibrary(vc: vc)
        }
        let cancel = UIAlertAction(title: Strings.General.cancel.capitalized, style: .cancel, handler: nil)
        alert.addAction(camera)
        alert.addAction(gallery)
        alert.addAction(cancel)
        vc.present(alert, animated: true, completion: nil)
    }
    
    func openCamera(vc: UIViewController) {
        
        if UIImagePickerController.isSourceTypeAvailable(.camera) == false {
            
            let alert = UIAlertController(title: Strings.Alerts.Titles.cameraNotFound, message: Strings.Alerts.Messages.cameraAccess, preferredStyle: .alert)
            let ok = UIAlertAction(title: Strings.General.ok.capitalized, style: .default, handler: nil)
            alert.addAction(ok)
            let settings = UIAlertAction(title: Strings.General.settings.capitalized, style: .cancel) { (action: UIAlertAction) in
                
                guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                    return
                }
                if UIApplication.shared.canOpenURL(settingsUrl) {
                    UIApplication.shared.open(settingsUrl, options: [:], completionHandler: nil)
                }
            }
            alert.addAction(ok)
            alert.addAction(settings)
            vc.present(alert, animated: true, completion: nil)
            
        } else {
            
            if picker == nil {
                
                let alert = UIAlertController(title: Strings.Alerts.Titles.error.capitalized, message: Strings.Alerts.Messages.general, preferredStyle: .alert)
                let ok = UIAlertAction(title: Strings.General.ok.capitalized, style: .cancel, handler: nil)
                alert.addAction(ok)
                vc.present(alert, animated: true, completion: nil)

            } else {
                
                picker?.delegate = self
                picker?.allowsEditing = false
                picker?.sourceType = .camera
                picker?.cameraCaptureMode = .photo
                vc.present(picker!, animated: true, completion: nil)
            }
        }
    }
    
    func openPhotoLibrary(vc: UIViewController) {
        
        if UIImagePickerController.isSourceTypeAvailable(.camera) == false {
            
            let alert = UIAlertController(title: Strings.Alerts.Titles.photoLibraryNotFound, message: Strings.Alerts.Messages.photoLibraryAccess, preferredStyle: .alert)
            let ok = UIAlertAction(title: Strings.General.ok.capitalized, style: .default, handler: nil)
            alert.addAction(ok)
            let settings = UIAlertAction(title: Strings.General.settings.capitalized, style: .cancel) { (action: UIAlertAction) in
                
                guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                    return
                }
                if UIApplication.shared.canOpenURL(settingsUrl) {
                    UIApplication.shared.open(settingsUrl, options: [:], completionHandler: nil)
                }
            }
            alert.addAction(ok)
            alert.addAction(settings)
            vc.present(alert, animated: true, completion: nil)
            
        } else {
            
            if picker == nil {
                
                let alert = UIAlertController(title: Strings.Alerts.Titles.error.capitalized, message: Strings.Alerts.Messages.general, preferredStyle: .alert)
                let ok = UIAlertAction(title: Strings.General.ok.capitalized, style: .cancel, handler: nil)
                alert.addAction(ok)
                vc.present(alert, animated: true, completion: nil)
                
            } else {
                picker?.delegate = self
                picker?.allowsEditing = false
                picker?.sourceType = .photoLibrary
                picker?.navigationBar.topItem?.rightBarButtonItem?.tintColor = .black
                vc.present(picker!, animated: true, completion: nil)
            }
        }
    }
    
    // MARK: - UI Image Picker Controller Delegate
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {

        let chosenImg: UIImage? = info[UIImagePickerControllerOriginalImage] as? UIImage
        if delegate != nil {
            delegate?.pictureChosen(img: chosenImg)
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        picker.dismiss(animated: true, completion: nil)
    }
}
