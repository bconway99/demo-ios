//
//  MapHelper.swift
//  Demo iOS
//
//  Created by Ben Conway on 13/05/2018.
//  Copyright © 2018 Ben Conway. All rights reserved.
//

import Foundation
import MapKit

class MapHelper: NSObject {

    static func getDirections(pub: Pub, completionHandler: @escaping (_ success: Bool) -> Void) {
        
        // Fail-safe.
        if pub.pubName == nil || pub.pubAddress == nil {
            completionHandler(false)
        }
        
        // Geocode the pub address to get it's coordinates.
        let geocoder = CLGeocoder()
        geocoder.geocodeAddressString(pub.pubAddress!) { (placemarks: [CLPlacemark]?, error: Error?) in
            
            if error != nil || placemarks == nil || placemarks!.count < 1 {
                
                print("Error: \(error!.localizedDescription)")
                completionHandler(false)
                
            } else {
                
                let clPlacemark: CLPlacemark? = placemarks!.last
                if
                    clPlacemark!.addressDictionary == nil ||
                    clPlacemark!.addressDictionary as? [String : Any] == nil ||
                    clPlacemark!.location == nil
                {

                    completionHandler(false)
                    
                } else {
                    
                    // Create the Map Kit Placemark from the Core Location Placemark.
                    let addrDict = clPlacemark!.addressDictionary as! [String : Any]
                    let coordinate = clPlacemark!.location!.coordinate
                    let mkPlacemark = MKPlacemark(coordinate: coordinate, addressDictionary: addrDict)

                    // Open the maps app with directions to the chosen pub.
                    let mapItem = MKMapItem(placemark: mkPlacemark)
                    mapItem.name = pub.pubName!
                    let launchOptions = [MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeWalking]
                    mapItem.openInMaps(launchOptions: launchOptions)
                    completionHandler(true)
                }
            }
        }
    }
}
