//
//  UserDefsHelper.swift
//  Demo iOS
//
//  Created by Ben Conway on 20/03/2018.
//  Copyright © 2018 Ben Conway. All rights reserved.
//

import UIKit

class UserDefsHelper: NSObject {

    // MARK: - Tokens
    
    class Tokens {
        
        // Keys.
        
        class Keys {
            
            static let accessToken: String = "accessToken"
        }
        
        // Methods.
        
        static func saveAccessToken(accessToken: String?) {
            
            if accessToken != nil {
                UserDefaults.standard.set(accessToken!, forKey: UserDefsHelper.Tokens.Keys.accessToken)
            }
        }
        
        static func getAccessToken() -> String? {
            
            let token = UserDefaults.standard.value(forKey: UserDefsHelper.Tokens.Keys.accessToken)
            if token == nil || token as? String == nil {
                return nil
            } else {
                return token! as? String
            }
        }
        
        static func clearAccessToken(completionHandler: @escaping (_ success: Bool) -> Void) {
            
            let keysArray: Array<String> = [UserDefsHelper.Tokens.Keys.accessToken]
            for key in keysArray {
                UserDefaults.standard.set(nil, forKey: key)
            }
            completionHandler(true)
        }
    }
    
    // MARK: - Sign In
    
    class SignIn {
        
        // Keys.
        
        class Keys {
            
            static let isSignedIn: String = "isSignedIn"
        }
        
        // Methods.
        
        static func setSignIn(signedIn: Bool) {
            
            UserDefaults.standard.set(signedIn, forKey: UserDefsHelper.SignIn.Keys.isSignedIn)
        }
        
        static func isSignedIn() -> Bool {
            
            let signedIn = UserDefaults.standard.value(forKey: UserDefsHelper.SignIn.Keys.isSignedIn)
            if signedIn == nil || signedIn as? Bool == nil {
                return false
            } else {
                
                if signedIn! as! Bool == false {
                    return false
                } else {
                    return true
                }
            }
        }
    }
}
