//
//  LogoutHelper.swift
//  Demo iOS
//
//  Created by Ben Conway on 14/05/2018.
//  Copyright © 2018 Ben Conway. All rights reserved.
//

import UIKit

class LogoutHelper: NSObject {

    static func logout(completionHandler: @escaping (_ success: Bool) -> Void) {
        
        UserDefsHelper.SignIn.setSignIn(signedIn: false)
        UserDefsHelper.Tokens.clearAccessToken(completionHandler: { (success: Bool) in
            MagicalRecordHelper.clearData(completionHandler: { (success: Bool) in
                DispatchQueue.main.async {
                    completionHandler(true)
                }
            })
        })
    }
}
