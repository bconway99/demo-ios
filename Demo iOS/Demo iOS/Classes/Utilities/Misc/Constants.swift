//
//  Constants.swift
//  Demo iOS
//
//  Created by Ben Conway on 20/02/2018.
//  Copyright © 2018 Ben Conway. All rights reserved.
//

import UIKit

class Constants: NSObject {

    // MARK: - API
    
    class TCPAPI {
    
        static let http: String = "http"
        static let https: String = "https"
        
        static let authorization: String = "{AUTH_STRING}"

        // Deployment environments.
        
        static let domainDev: String = "{DEV_ENDPOINT}"
        static let domainQAT: String = ""
        static let domainUAT: String = ""
        static let domainProd: String = ""
        
        // MARK: Endpoints
        
        class Endpoints {
        
            static let authorize: String = "authorize"
            static let drink: String = "drink"
            static let login: String = "login"
            static let name: String = "name"
            static let photo: String = "photo"
            static let pub: String = "pub"
            static let pubDrink: String = "pub-drink"
            static let search: String = "search"
            static let social: String = "social"
        }
        
        // MARK: - Distance Units
        
        class DistanceUnits {
            
            static let kilometres: String = "km"
            static let miles: String = "m"
        }
    }
    
    // MARK: - Google
    
    class Google {
        
        static let APIKey: String = "{API_KEY}"
    }
    
    // MARK: - Facebook
    
    class Facebook {
        
        static let appID: String = "{APP_ID}"
    }    
}
