//
//  MagicalRecordHelper.swift
//  Demo iOS
//
//  Created by Ben Conway on 18/03/2018.
//  Copyright © 2018 Ben Conway. All rights reserved.
//

import Foundation
import MagicalRecord

class MagicalRecordHelper: NSObject {
    
    // Set up the data model.
    static func setupDataModel() {
        
        // If the data model filename is identical to the project's name, then we can just call the below method.
        MagicalRecord.setupCoreDataStack()
    }
    
    // Clean up the data model.
    static func cleanUp() {
        
        MagicalRecord.cleanUp()
    }
    
    // Get the managed object context so that we can always save, fetch and delete under the same one.
    static func getContext() -> NSManagedObjectContext {
        
        return NSManagedObjectContext.mr_default()
    }
    
    // Get the managed object model.
    static func getModel() -> NSManagedObjectModel? {
        
        if NSManagedObjectModel.mr_default() == nil {
            return nil
            
        } else {
            return NSManagedObjectModel.mr_default()!
        }
    }
    
    // Clear all of the managed objects for each Core Data entity.
    static func clearData(completionHandler: @escaping (_ success: Bool) -> Void) {
        
        let managedObjModel: NSManagedObjectModel? = MagicalRecordHelper.getModel()
        if managedObjModel != nil && managedObjModel!.entities.count > 0 {
            
            let allEntities = managedObjModel!.entities
            for entity in allEntities {
                
                if entity.managedObjectClassName != nil {
                    
                    let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entity.managedObjectClassName!)
                    let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
                    
                    do {
                        _ = try MagicalRecordHelper.getContext().execute(deleteRequest)
                    } catch let error {
                        print("Delete entity error: \(error.localizedDescription)")
                    }
                }
            }
        }
        
        completionHandler(true)
    }
    
    // Clear all instances of the User entity, however there should only ever be one instance so this is a fail-safe.
    static func clearUsers(completionHandler: @escaping (_ success: Bool) -> Void) {
        
        User.mr_truncateAll()
        MagicalRecordHelper.getContext().mr_saveToPersistentStoreAndWait()
        completionHandler(true)
    }
    
    // Clear all instances of any saved Pub entities.
    static func clearPubs(completionHandler: @escaping (_ success: Bool) -> Void) {
        
        Pub.mr_truncateAll()
        MagicalRecordHelper.getContext().mr_saveToPersistentStoreAndWait()
        completionHandler(true)
    }
}
