//
//  FirebaseHelper.swift
//  Demo iOS
//
//  Created by Ben Conway on 07/05/2018.
//  Copyright © 2018 Ben Conway. All rights reserved.
//

import Foundation
import Firebase

class FirebaseHelper: NSObject {

    // Configure the Firebase app.
    static func configure() {
        
        FirebaseApp.configure()
    }
    
    // Authenticate the user using their Facebook access token.
    static func authFacebook(accessToken: String, completionHandler: @escaping (_ success: Bool, _ token: String?) -> Void) {
        
        // Exchange the Facebook access token for a Firebase credential.
        let credential = FacebookAuthProvider.credential(withAccessToken: accessToken)
        // Attempt to sign the user in.
        Auth.auth().signIn(with: credential) { (user, error) in
            
            if error != nil || user == nil { //}|| user.to {
                
                // There was an error so show this to the user.
                print("Error: \(error!.localizedDescription)")
                completionHandler(false, nil)
            }
            
            // Try and decode the ID token.
            user!.getIDToken(completion: { (tokenStr: String?, idError: Error?) in
                
                if idError != nil || tokenStr == nil {
                    
                    // There was an error so show this to the user.
                    print("Error: \(error!.localizedDescription)")
                    completionHandler(false, nil)
                    
                } else {
                    
                    
                    // User signed in successfully.
                    completionHandler(true, tokenStr!)
                }
            })
        }
    }
}
