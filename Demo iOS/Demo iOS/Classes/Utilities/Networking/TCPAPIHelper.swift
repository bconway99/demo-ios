//
//  TCPAPIHelper.swift
//  Demo iOS
//
//  Created by Ben Conway on 12/03/2018.
//  Copyright © 2018 Ben Conway. All rights reserved.
//

import Foundation
import Alamofire
import JWTDecode

class TCPAPIHelper: NSObject {
    
    // Use this class for handling project specific networking code such as:
    //
    // • Making API calls using the HTTPRequests class.
    // • Parsing project specific JSON responses.
    // • Changing the environment type and or domain.
    // • Changing the transfer protocol.
    
    static let transferProtocol: TransferProtocol = TransferProtocol.https
    static let deploymentEnvironment: DeploymentEnvironment = DeploymentEnvironment.Development
    static let domain: String = TCPAPIHelper.General.createDomain(transferProtocol: TCPAPIHelper.transferProtocol, deploymentEnvironment: TCPAPIHelper.deploymentEnvironment)
    
    // MARK: - General
    
    class General {
        
        // Construct the API URL based on the current domain and service.
        static func getUrl(service: String) -> String {
            
            return "\(TCPAPIHelper.domain)\(service)"
        }
        
        static func createDomain(transferProtocol: TransferProtocol, deploymentEnvironment: DeploymentEnvironment) -> String {
            
            // Assign the transfer protocol.
            
            var transferProtocolStr: String = ""
            switch transferProtocol {
                
            case TransferProtocol.http:
                transferProtocolStr = Constants.TCPAPI.http
                break
                
            case TransferProtocol.https:
                transferProtocolStr = Constants.TCPAPI.https
                break
            }
            
            // Assign the domain relevant to the current environment type.
            
            var domainStr: String = ""
            switch deploymentEnvironment {
                                
            case DeploymentEnvironment.Development:
                domainStr = Constants.TCPAPI.domainDev
                break
                
            case DeploymentEnvironment.QAT:
                domainStr = Constants.TCPAPI.domainQAT
                break
                
            case DeploymentEnvironment.UAT:
                domainStr = Constants.TCPAPI.domainUAT
                break
                
            case DeploymentEnvironment.Production:
                domainStr = Constants.TCPAPI.domainProd
                break
            }
            
            return "\(transferProtocolStr)://\(domainStr)/"
        }
    
        static func getHeaders(useToken: Bool, isFile: Bool) -> HTTPHeaders? {

            // When sending up files (images), we need a different content type.
            var contentType: String = "application/json"
            if isFile == true {
                contentType = "multipart/form-data"
            }
            
            // We don't want to use the token for every request.
            // Or sometimes the token is sent up in the body rather than in the headers.
            if useToken == false {
                
                return ["Content-Type" : contentType]

            } else {
                
                // If there is a valid access token then return the headers.
                let accessToken: String? = UserDefsHelper.Tokens.getAccessToken()
                if accessToken == nil {
                    return nil
                } else {
                    
                    return [
                        "Authorization" : accessToken!,
                        "Content-Type" : contentType
                    ]
                }
            }
        }
        
        static func checkSession(json: [String : Any]?) {

            // Check the session object of the response for the access token and whether the user should be logged out.
            if json != nil {
             
                let isLoggedIn: Bool? = json![JSONKeys.isLoggedIn] as? Bool
                if isLoggedIn == nil || isLoggedIn == false {
                
                    // TODO: Log out the user and clear their data.
                    print("LOG OUT user!")
                    
                } else {
                    
                    // Save the access token.
                    
                    let token: String? = json![JSONKeys.userAccessToken] as? String
                    if token != nil {
                        UserDefsHelper.Tokens.saveAccessToken(accessToken: token!)
                    }
                }
            }
        }
    }
    
    // MARK: - API Calls
    
    class APICalls {
        
        // MARK: - Login API

        class LoginAPI {
            
            static func authorize(completionHandler: @escaping (_ success: Bool) -> Void) {
                
                let url: String = TCPAPIHelper.domain + Constants.TCPAPI.Endpoints.authorize
                HTTPRequests.get(url: url, params: nil, headers: TCPAPIHelper.General.getHeaders(useToken: true, isFile: false)) { (success: Bool, resp: DataResponse<Any>?) in
                    
                    if resp == nil {
                        completionHandler(false)
                    }
                    
                    do {
                        
                        let json = try JSONSerialization.jsonObject(with: resp!.data!, options: []) as? [String : AnyObject]
                        // We need the JWT (JSON web token), to decode and get the user's details.
                        // We also need the user type which is sent separate from the JWT.
                        let token: String? = json![JSONKeys.token] as? String
                        let userType: String? = json![JSONKeys.userType] as? String
                        if token == nil || userType == nil {
                            completionHandler(false)
                        } else {
                            
                            // Get the user's details from the JWT (JSON web token).
                            // Also decode the user type from a base64 string.
                            let userDict = JWTHelper.decodeJWT(token: token!)
                            if userDict == nil || userType == nil {
                                completionHandler(false)
                            } else {
                                
                                // First clear any current users before saving a new one.
                                MagicalRecordHelper.clearUsers(completionHandler: { (success: Bool) in
                                    
                                    // If the JWT (JSON web token) was decoded successfully then create and save the user object.
                                    let context = MagicalRecordHelper.getContext()
                                    let user = User.fromJSON(object: userDict!, userType: userType!, context: context)
                                    if user == nil {
                                        completionHandler(false)
                                    } else {
                                        
                                        context.mr_saveToPersistentStoreAndWait()
                                        UserDefsHelper.Tokens.saveAccessToken(accessToken: token!)
                                        completionHandler(true)
                                    }
                                })
                            }
                        }

                    } catch let error {
                        
                        print("JSON error: \(error.localizedDescription)")
                        completionHandler(false)
                    }
                }
            }
            
            static func login(token: String, completionHandler: @escaping (_ success: Bool) -> Void) {
            
                let params: Parameters = [
                    
                    JSONKeys.token : token
                ]
                
                let url: String = TCPAPIHelper.domain + Constants.TCPAPI.Endpoints.login
                HTTPRequests.post(url: url, params: params, headers: TCPAPIHelper.General.getHeaders(useToken: false, isFile: false)) { (success: Bool, resp: DataResponse<Any>?) in
                    
                    if resp == nil {
                        completionHandler(false)
                    }
                    
                    do {
                        
                        let json = try JSONSerialization.jsonObject(with: resp!.data!, options: []) as? [String : AnyObject]
                        // We need the JWT (JSON web token), to decode and get the user's details.
                        // We also need the user type which is separate to the JWT.
                        let token: String? = json![JSONKeys.token] as? String
                        let userType: String? = json![JSONKeys.userType] as? String
                        if token == nil || userType == nil {
                            completionHandler(false)
                        } else {
                            
                            // Get the user's details from the JWT (JSON web token).
                            let userDict = JWTHelper.decodeJWT(token: token!)
                            if userDict == nil {
                                completionHandler(false)
                            } else {
                             
                                // First clear any current users before saving a new one.
                                MagicalRecordHelper.clearUsers(completionHandler: { (success: Bool) in
                                    
                                    // If the JWT (JSON web token) was decoded successfully then create and save the user object.
                                    let context = MagicalRecordHelper.getContext()
                                    let user = User.fromJSON(object: userDict!, userType: userType!, context: context)
                                    if user == nil {
                                        completionHandler(false)
                                    } else {
                                        
                                        context.mr_saveToPersistentStoreAndWait()
                                        UserDefsHelper.SignIn.setSignIn(signedIn: true)
                                        UserDefsHelper.Tokens.saveAccessToken(accessToken: token!)
                                        completionHandler(true)
                                    }
                                })
                            }
                        }
                        
                    } catch let error {
                        
                        print("JSON error: \(error.localizedDescription)")
                        completionHandler(false)
                    }
                }
            }
        }
        
        // MARK: - Search API
    
        class SearchAPI {
            
            static func search(searchRequest: PubSearchRequest, completionHandler: @escaping (_ success: Bool) -> Void) {
                
                let url: String = TCPAPIHelper.domain + Constants.TCPAPI.Endpoints.search
                HTTPRequests.post(url: url, params: searchRequest.toParams(), headers: TCPAPIHelper.General.getHeaders(useToken: true, isFile: false)) { (success: Bool, resp: DataResponse<Any>?) in
                    
                    if resp == nil {
                        completionHandler(false)
                    }
                    
                    do {
                        
                        let json = try JSONSerialization.jsonObject(with: resp!.data!, options: []) as? [String : AnyObject]
                        TCPAPIHelper.General.checkSession(json: json![JSONKeys.session] as? [String : Any])
                        let pubsArray: [[String : Any]]? = json![JSONKeys.pubs] as? [[String : Any]]
                        if pubsArray == nil || pubsArray!.count < 1 {
                            completionHandler(false)
                        } else {
                            
                            // First clear all current pubs before saving a new ones.
                            Pub.mr_truncateAll()

                            let context = MagicalRecordHelper.getContext()
                            
                            var pubs: [Pub] = []
                            for pubObj in pubsArray! {
                                
                                let pub = Pub.fromJSON(object: pubObj, context: context)
                                if pub != nil {
                                    pubs.append(pub!)
                                }
                            }
                            
                            if pubs.count > 0 {
                                context.mr_saveToPersistentStoreAndWait()
                            }
                            
                            completionHandler(true)
                        }
                        
                    } catch let error {
                        
                        print("JSON error: \(error.localizedDescription)")
                        completionHandler(false)
                    }
                }
            }
            
            static func searchDrinkName(drinkName: String, completionHandler: @escaping (_ success: Bool, _ drinkAutoResps: [DrinkAutoResp]?) -> Void) {
                
                let url: String = TCPAPIHelper.domain + Constants.TCPAPI.Endpoints.search + "/" + Constants.TCPAPI.Endpoints.drink + "/" + drinkName
                HTTPRequests.get(url: url, params: nil, headers: TCPAPIHelper.General.getHeaders(useToken: true, isFile: false)) { (success: Bool, resp: DataResponse<Any>?) in
                    
                    if resp == nil {
                        completionHandler(false, nil)
                    }
                    
                    do {
                        
                        let json = try JSONSerialization.jsonObject(with: resp!.data!, options: []) as? [String : AnyObject]
                        TCPAPIHelper.General.checkSession(json: json![JSONKeys.session] as? [String : Any])
                        let drinkArray: [[String : Any]]? = json![JSONKeys.drinks] as? [[String : Any]]
                        if drinkArray == nil {
                            completionHandler(false, nil)
                        } else {
                            
                            var drinkAutoResps: [DrinkAutoResp] = []
                            for drinkAuto in drinkArray! {
                                
                                let drinkName = drinkAuto[JSONKeys.drinkName] as? String
                                let drinkID = drinkAuto[JSONKeys.drinkID] as? String
                                let drinkType = drinkAuto[JSONKeys.drinkType] as? String
                                let drinkParent = drinkAuto[JSONKeys.drinkParent] as? String
                                // Make sure the response has at least one item per drink.
                                if drinkName != nil && drinkID != nil && drinkType != nil && drinkParent != nil {
                                    
                                    let drinkAutoResp = DrinkAutoResp(drinkName: drinkName, drinkID: drinkID, drinkType: drinkType, drinkParent: drinkParent!)
                                    drinkAutoResps.append(drinkAutoResp)
                                }
                            }
                            
                            if drinkAutoResps.count < 1 {
                                completionHandler(false, nil)
                            } else {
                                
                                // Filter out any drinks with nil variables or empty strings.
                                drinkAutoResps = drinkAutoResps.filter {
                                   
                                    $0.drinkID != nil && $0.drinkName != nil &&
                                    $0.drinkType != nil && $0.drinkParent != nil &&
                                    $0.drinkID!.count > 0 && $0.drinkName!.count > 0 &&
                                    $0.drinkType!.count > 0 &&  $0.drinkParent!.count > 0
                                }
                                completionHandler(true, drinkAutoResps)
                            }
                        }
                        
                    } catch let error {
                        
                        print("JSON error: \(error.localizedDescription)")
                        completionHandler(false, nil)
                    }
                }
            }
        }
        
        // MARK: - Pub API
        
        class PubAPI {
            
            static func getPub(pub: Pub, completionHandler: @escaping (_ success: Bool, _ thePub: Pub?) -> Void) {
                
                // Fail-safe.
                if pub.pubID == nil {
                    completionHandler(false, nil)
                    return
                }
                
                let url: String = TCPAPIHelper.domain + Constants.TCPAPI.Endpoints.pub + "/" + pub.pubID!
                HTTPRequests.get(url: url, params: nil, headers: TCPAPIHelper.General.getHeaders(useToken: true, isFile: false)) { (success: Bool, resp: DataResponse<Any>?) in
                    
                    if resp == nil {
                        completionHandler(false, nil)
                    }
                    
                    do {
                        
                        let json = try JSONSerialization.jsonObject(with: resp!.data!, options: []) as? [String : AnyObject]
                        
                        TCPAPIHelper.General.checkSession(json: json![JSONKeys.session] as? [String : Any])
                        let pubObj: [String : Any]? = json![JSONKeys.pub] as? [String : Any]
                        if pubObj == nil {
                            completionHandler(false, nil)
                        } else {
                            
                            let context = MagicalRecordHelper.getContext()
                            let pub = Pub.fromJSON(object: pubObj, context: context)
                            if pub == nil {
                                completionHandler(false, nil)
                            } else {
                                context.mr_saveToPersistentStoreAndWait()
                                completionHandler(true, pub!)
                            }
                        }
                        
                    } catch let error {
                        
                        print("JSON error: \(error.localizedDescription)")
                        completionHandler(false, nil)
                    }
                }
            }
            
            static func addPub(addPubRequest: AddPubRequest, completionHandler: @escaping (_ success: Bool) -> Void) {
                
                let url: String = TCPAPIHelper.domain + Constants.TCPAPI.Endpoints.pub
                HTTPRequests.post(url: url, params: addPubRequest.toParams(), headers: TCPAPIHelper.General.getHeaders(useToken: true, isFile: false)) { (success: Bool, resp: DataResponse<Any>?) in
                    
                    if resp == nil {
                        completionHandler(false)
                    }
                    
                    do {
                        
                        let json = try JSONSerialization.jsonObject(with: resp!.data!, options: []) as? [String : AnyObject]
                        // TODO: Handle the app if a user makes a successful API call.
                        print("HERE!")
                        
                    } catch let error {
                        
                        print("JSON error: \(error.localizedDescription)")
                        completionHandler(false)
                    }
                }
            }
            
            static func updatePub(updatePubRequest: UpdatePubRequest, completionHandler: @escaping (_ success: Bool) -> Void) {
                
                let url: String = TCPAPIHelper.domain + Constants.TCPAPI.Endpoints.pub
                HTTPRequests.put(url: url, params: updatePubRequest.toParams(), headers: TCPAPIHelper.General.getHeaders(useToken: true, isFile: false)) { (success: Bool, resp: DataResponse<Any>?) in
                    
                    if resp == nil {
                        completionHandler(false)
                    }
                    
                    do {
                        
                        let json = try JSONSerialization.jsonObject(with: resp!.data!, options: []) as? [String : AnyObject]
                        // TODO: Handle the app if a user makes a successful API call.
                        print("HERE!")
                        
                    } catch let error {
                        
                        print("JSON error: \(error.localizedDescription)")
                        completionHandler(false)
                    }
                }
            }
            
            static func addDrink(addDrinkRequest: AddDrinkRequest, completionHandler: @escaping (_ success: Bool) -> Void) {
                
                let url: String = TCPAPIHelper.domain + Constants.TCPAPI.Endpoints.pubDrink
                HTTPRequests.put(url: url, params: addDrinkRequest.toParams(), headers: TCPAPIHelper.General.getHeaders(useToken: true, isFile: false)) { (success: Bool, resp: DataResponse<Any>?) in
                    
                    if resp == nil {
                        completionHandler(false)
                    }
                    
                    do {
                        
                        let json = try JSONSerialization.jsonObject(with: resp!.data!, options: []) as? [String : AnyObject]
                        // TODO: Handle the app if a user makes a successful API call.
                        print("HERE!")
                        
                    } catch let error {
                        
                        print("JSON error: \(error.localizedDescription)")
                        completionHandler(false)
                    }
                }
            }
            
            static func addPubPhoto(pubID: String, pubImg: UIImage, completionHandler: @escaping (_ success: Bool) -> Void) {
                
                let params: Parameters = [
                    JSONKeys.pubID : pubID
                ]
                
                let fileData: Data? = UIImageJPEGRepresentation(pubImg, 1.0)
                if fileData == nil {
                    return
                }
                
                let url: String = TCPAPIHelper.domain + Constants.TCPAPI.Endpoints.photo
                HTTPRequests.putImg(url: url, params: params, img: pubImg, headers: TCPAPIHelper.General.getHeaders(useToken: true, isFile: true)) { (success: Bool, resp: DataResponse<Any>?) in
                    
                    print("HERE!")
                }
            }
        }
    }
}
