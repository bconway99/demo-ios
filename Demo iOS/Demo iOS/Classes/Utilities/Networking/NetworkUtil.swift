//
//  NetworkUtil.swift
//  Demo iOS
//
//  Created by Ben Conway on 12/03/2018.
//  Copyright © 2018 Ben Conway. All rights reserved.
//

import UIKit

class NetworkUtil: NSObject {
    
    static func setNetworkActivityIndicator(visible: Bool) {
        
        if visible == true {
            
            // All UI updates must happen on the main thread.
            DispatchQueue.main.async {
                UIApplication.shared.isNetworkActivityIndicatorVisible = true
            }
            
        } else {
            
            // All UI updates must happen on the main thread.
            DispatchQueue.main.async {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
            }
        }
    }
}
