//
//  LocationHelper.swift
//  Demo iOS
//
//  Created by Ben Conway on 19/03/2018.
//  Copyright © 2018 Ben Conway. All rights reserved.
//

import Foundation
import CoreLocation

protocol LocationHelperDelegate: class {
    
    func locAuthStatus(enabled: Bool)
    func locationUpdated(coordinate: CLLocationCoordinate2D)
    func headerUpdated()
}

class LocationHelper: NSObject, CLLocationManagerDelegate {
    
    weak var delegate: LocationHelperDelegate?
    var canUpdateLoc: Bool = false
    var locManager: CLLocationManager = {
        
        let manager = CLLocationManager()
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.activityType = .fitness
        manager.distanceFilter = 2.0 // Specifies the minimum update distance in meters.
        manager.pausesLocationUpdatesAutomatically = true // Specifies that location updates may automatically be paused when possible.
        return manager
    }()
    
    override init() {
        
        super.init()
        locManager.delegate = self
    }
    
    // MARK: - General
    
    static func isEnabled() -> Bool {
        
        return CLLocationManager.locationServicesEnabled()
    }
    
    static func isAuthorised() -> Bool {
        
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            return true
        } else {
            return false
        }
    }
    
    // MARK: - Get User Location
    
    func requestAuth() {
        
        locManager.requestWhenInUseAuthorization()
    }
    
    func startUpdating() {
        
        canUpdateLoc = true
        locManager.delegate = self
        locManager.startUpdatingHeading()
        locManager.startUpdatingLocation()
    }
    
    func stopUpdating() {
        
        canUpdateLoc = false
        locManager.stopUpdatingHeading()
        locManager.stopUpdatingLocation()
    }
    
    // MARK: - CL Location Manager Delegate
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        if delegate != nil {
            if status != .authorizedWhenInUse {
                delegate!.locAuthStatus(enabled: false)
            } else {
                delegate!.locAuthStatus(enabled: true)
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    
        if canUpdateLoc == true {
            
            canUpdateLoc = false
            if locations.count > 0 {
        
                let location: CLLocation = locations.last!
                let coordinate: CLLocationCoordinate2D = location.coordinate
                if delegate != nil {
                    delegate!.locationUpdated(coordinate: coordinate)
                    stopUpdating()
                }
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateHeading newHeading: CLHeading) {
    
        if delegate != nil {
            delegate!.headerUpdated()
        }
    }
    
    func locationManagerDidPauseLocationUpdates(_ manager: CLLocationManager) {
    }
    
    func locationManagerDidResumeLocationUpdates(_ manager: CLLocationManager) {
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
    }
}
