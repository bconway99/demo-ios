//
//  JSONKeys.swift
//  Demo iOS
//
//  Created by Ben Conway on 29/04/2018.
//  Copyright © 2018 Ben Conway. All rights reserved.
//

import UIKit

class JSONKeys: NSObject {

    static let authorization: String = "authorization"
    static let category: String = "category"
    static let closing: String = "closing"
    static let data: String = "data"
    static let distance: String = "distance"
    static let distanceUnits: String = "distance_units"
    static let drink: String = "drink"
    static let drinks: String = "drinks"
    static let drinkID: String = "drink_id"
    static let drinkName: String = "drink_name"
    static let drinkParent: String = "drink_parent"
    static let drinkPrices: String = "drink_prices"
    static let drinkType: String = "drink_type"
    static let drinkAutoComplete: String = "drink_auto_complete"
    static let email: String = "email"
    static let emailVerified: String = "email_verified"
    static let isLoggedIn: String = "is_logged_in"
    static let latitude: String = "latitude"
    static let longitude: String = "longitude"
    static let measurement: String = "measurement"
    static let menuCategory: String = "menu_category"
    static let menuSubcategory: String = "menu_subcategory"
    static let name: String = "name"
    static let opening: String = "opening"
    static let options: String = "options"
    static let picture: String = "picture"
    static let price: String = "price"
    static let prices: String = "prices"
    static let pubAddress: String = "pub_address"
    static let pubCity: String = "pub_city"
    static let pubCountry: String = "pub_country"
    static let pubCoordinates: String = "pub_coordinates"
    static let pubDistance: String = "pub_distance"
    static let pubFeatures: String = "pub_features"
    static let pubFranchise: String = "pub_franchise"
    static let pubID: String = "pub_id"
    static let pubInformation: String = "pub_information"
    static let pubLatitude: String = "pub_latitude"
    static let pubLongitude: String = "pub_longitude"
    static let pubName: String = "pub_name"
    static let pub: String = "pub"
    static let pubs: String = "pubs"
    static let pubTime: String = "pub_time"
    static let pubTimes: String = "pub_times"
    static let pubWebsite: String = "pub_website"
    static let response: String = "response"
    static let search: String = "search"
    static let searchType: String = "search_type"
    static let session: String = "session"
    static let socialNetwork: String = "social_network"
    static let socialUserID: String = "social_user_id"
    static let sub: String = "sub"
    static let subcategory: String = "subcategory"
    static let token: String = "token"
    static let userAccessToken: String = "user_access_token"
    static let userID: String = "user_id"
    static let userLocation: String = "user_location"
    static let userType: String = "user_type"
    
    // MARK: - Pub Filter
    
    class PubFeatures {
        
        static let familyFriendly: String = "family_friendly"
        static let food: String = "food"
        static let liveMusic: String = "live_music"
        static let liveSport: String = "live_sport"
        static let liveTV: String = "live_tv"
        static let outdoorSeating: String = "outdoor_seating"
        static let parking: String = "parking"
        static let petFriendly: String = "pet_friendly"
        static let pool: String = "pool"
        static let quiz: String = "quiz"
        static let stepFree: String = "step_free"
        static let wifi: String = "wifi"
    }
    
    // MARK: - Pub Days
    
    class PubDays {
        
        static let monday: String = "monday"
        static let tuesday: String = "tuesday"
        static let wednesday: String = "wednesday"
        static let thursday: String = "thursday"
        static let friday: String = "friday"
        static let saturday: String = "saturday"
        static let sunday: String = "sunday"
    }
    
    // MARK: - Search Types
    
    class SearchTypes {
        
        static let cheapestPint: String = "cheapest_pint"
        static let cheapestDrink: String = "cheapest_drink"
        static let cheapestType: String = "cheapest_type"
        static let closestPub: String = "closest_pub"
    }
}
