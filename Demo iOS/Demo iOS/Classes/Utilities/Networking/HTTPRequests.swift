//
//  HTTPRequests.swift
//  Demo iOS
//
//  Created by Ben Conway on 12/03/2018.
//  Copyright © 2018 Ben Conway. All rights reserved.
//

import Foundation
import Alamofire

class HTTPRequests: NSObject {
    
    // This class handles the basic HTTP requests via Alamofire and can be used in any and all projects that use this framework.
    // DO NOT add any code in here that is specific to a project, instead create another class for that and call these methods from there.
    
    static let timeoutSecs: TimeInterval = 10.0
    
    // Get request.
    
    static func get(url: String, params: Parameters?, headers: HTTPHeaders?, completionHandler: @escaping (_ success: Bool, _ resp: DataResponse<Any>?) -> Void) {
        
        NetworkUtil.setNetworkActivityIndicator(visible: true)
        SessionManager.default.session.configuration.timeoutIntervalForRequest = HTTPRequests.timeoutSecs
        request(url, method: .get, parameters: params, encoding: URLEncoding.httpBody, headers: headers).responseJSON { (resp: DataResponse<Any>) in
            
            // Handle the immediate response, but leave the specific JSON parsing to the API call method.
            NetworkUtil.setNetworkActivityIndicator(visible: false)
            if resp.result.isSuccess == true {
                completionHandler(true, resp)
            } else {
                
                // Check if the failure was due to a timeout.
                if resp.error?._code == NSURLErrorTimedOut {
                    print("TIMEOUT!!!")
                }
                completionHandler(false, resp)
            }
        }
    }
    
    // Post request.
    
    static func post(url: String, params: Parameters?, headers: HTTPHeaders?, completionHandler: @escaping (_ success: Bool, _ resp: DataResponse<Any>?) -> Void) {
        
        NetworkUtil.setNetworkActivityIndicator(visible: true)
        SessionManager.default.session.configuration.timeoutIntervalForRequest = HTTPRequests.timeoutSecs
        request(url, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).responseJSON { (resp: DataResponse<Any>) in

            // Handle the immediate response, but leave the specific JSON parsing to the API call method.
            NetworkUtil.setNetworkActivityIndicator(visible: false)
            if resp.result.isSuccess == true {
                completionHandler(true, resp)
            } else {
                
                // Check if the failure was due to a timeout.
                if resp.error?._code == NSURLErrorTimedOut {
                    print("TIMEOUT!!!")
                }
                completionHandler(false, resp)
            }
        }
    }
    
    // Put request.
    
    static func put(url: String, params: Parameters?, headers: HTTPHeaders?, completionHandler: @escaping (_ success: Bool, _ resp: DataResponse<Any>?) -> Void) {
        
        NetworkUtil.setNetworkActivityIndicator(visible: true)
        SessionManager.default.session.configuration.timeoutIntervalForRequest = HTTPRequests.timeoutSecs
        request(url, method: .put, parameters: params, encoding: JSONEncoding.default, headers: headers).responseJSON { (resp: DataResponse<Any>) in
            
            // Handle the immediate response, but leave the specific JSON parsing to the API call method.
            NetworkUtil.setNetworkActivityIndicator(visible: false)
            if resp.result.isSuccess == true {
                completionHandler(true, resp)
            } else {
                
                // Check if the failure was due to a timeout.
                if resp.error?._code == NSURLErrorTimedOut {
                    print("TIMEOUT!!!")
                }
                completionHandler(false, resp)
            }
        }
    }
    
    static func putImg(url: String, params: Parameters?, img: UIImage?, headers: HTTPHeaders?, completionHandler: @escaping (_ success: Bool, _ resp: DataResponse<Any>?) -> Void) {
        
        NetworkUtil.setNetworkActivityIndicator(visible: true)
        SessionManager.default.session.configuration.timeoutIntervalForRequest = HTTPRequests.timeoutSecs
        
        // Fail-safe.
        let pubID: String? = params?[JSONKeys.pubID] as? String
        if pubID == nil {
            completionHandler(false, nil)
        }
        
        upload(multipartFormData: { (multipartFormData) in
            
            for (key, value) in params! {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            multipartFormData.append(UIImageJPEGRepresentation(img!, 1.0)!, withName: pubID!, fileName: "\(pubID!).jpeg", mimeType: "image/jpeg")
            
        }, usingThreshold: UInt64.init(), to: url, method: .put, headers: headers) { (result: SessionManager.MultipartFormDataEncodingResult) in

            print("HERE!")
        }
    }
}
