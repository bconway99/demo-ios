//
//  FacebookHelper.swift
//  Demo iOS
//
//  Created by Ben Conway on 20/02/2018.
//  Copyright © 2018 Ben Conway. All rights reserved.
//

import UIKit
import FacebookLogin
import FBSDKLoginKit

protocol FacebookHelperDelegate: class {
    
    func fbLoginSuccess(accessToken: String, dict: [String : Any])
    func fbLoginFailed()
    func fbLogout()
}

class FacebookHelper: NSObject {
    
    weak var delegate: FacebookHelperDelegate?
    static let params: [String] = [
    
        "id",
        "name",
        "email",
        "picture.type(large)"
    ]
    
    static var loginBtn: LoginButton {
        
        let btn = LoginButton(readPermissions: [.publicProfile, .email, .userFriends])
        btn.delegate = self as? LoginButtonDelegate
        return btn
    }
    
    // MARK: - Login
    
    func logIn(vc: UIViewController) {

        weak var weakSelf = self
        let loginManager = LoginManager()
        loginManager.logIn(readPermissions: [.publicProfile, .email, .userFriends], viewController: vc) { (loginResult: LoginResult) in

            switch loginResult {

            case .failed(let error):
                print(error)
                if weakSelf!.delegate != nil {
                    weakSelf!.delegate!.fbLoginFailed()
                }

            case .cancelled:
                if weakSelf!.delegate != nil {
                    weakSelf!.delegate!.fbLoginFailed()
                }

            case .success:
                weakSelf!.getProfileData()
            }
        }
    }

    func getProfileData() {

        if FBSDKAccessToken.current() == nil {
            
            // Set a fail-safe incase there is an issue with the access token.
            if delegate != nil {
                delegate!.fbLoginFailed()
            }
            
        } else {
            
            weak var weakSelf = self
            // Create a graph request to extract the user's profile data.
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": FacebookHelper.params.joined(separator: ",")]).start(completionHandler: { (connection, result, error) -> Void in
                
                if error != nil && result! as? [String : Any] != nil {
                
                    print("FBSDK graph request error: \(error!.localizedDescription)")
                    if weakSelf!.delegate != nil {
                        weakSelf!.delegate!.fbLoginFailed()
                    }
                    
                } else {

                    // Create a dictionary of the user's relevant data needed for the login API call.
                    let resultDict = result! as! [String : Any]
                    let userDict: [String : Any] = [
                        
                        JSONKeys.email : resultDict["email"]! as! String,
                        JSONKeys.socialNetwork : "Facebook",
                        JSONKeys.socialUserID : resultDict["id"]! as! String
                    ]
                    
                    // Get the Facebook access token for Firebase authentication.
                    let accessToken: String = FBSDKAccessToken.current().tokenString
                    if weakSelf!.delegate != nil {
                        weakSelf!.delegate!.fbLoginSuccess(accessToken: accessToken, dict: userDict)
                    }
                }
            })
        }
    }
}

// MARK: - Login Button Delegate

extension FacebookHelper: LoginButtonDelegate {
    
    // Called when the button was used to login and the process finished.
    func loginButtonDidCompleteLogin(_ loginButton: LoginButton, result: LoginResult) {
        
        print("Login success!")
    }
    
    // Called when the button was used to logout.
    func loginButtonDidLogOut(_ loginButton: LoginButton) {
        
        if delegate != nil {
            delegate!.fbLogout()
        }
    }
}
