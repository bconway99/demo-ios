//
//  JWTHelper.swift
//  Demo iOS
//
//  Created by Ben Conway on 18/03/2018.
//  Copyright © 2018 Ben Conway. All rights reserved.
//

import Foundation
import JWTDecode

class JWTHelper: NSObject {
    
    // Decode the JWT (JSON web token).
    static func decodeJWT(token: String) -> [String : Any]? {
        
        do {
            
            let jwt = try decode(jwt: token)
            return jwt.body
        
        } catch let error {
        
            print("JWT decode error: \(error.localizedDescription)")
            return nil
        }
    }
    
    // Check if the JWT (JSON web token) has expired.
    static func isExpired(token: String) -> Bool {
    
        do {
            
            let jwt = try decode(jwt: token)
            return jwt.expired
            
        } catch {
            
            return false
        }
    }
}
