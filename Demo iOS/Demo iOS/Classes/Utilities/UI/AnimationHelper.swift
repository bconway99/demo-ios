//
//  AnimationHelper.swift
//  Demo iOS
//
//  Created by Ben Conway on 11/08/2018.
//  Copyright © 2018 Ben Conway. All rights reserved.
//

import UIKit

class AnimationHelper: NSObject {
    
    // Turn the text field red, before returning it to it's original colour.
    static func shake(view: UIView) {
        
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.07
        animation.repeatCount = 4
        animation.autoreverses = true
        animation.fromValue = NSValue(cgPoint: CGPoint(x: view.center.x - 10, y: view.center.y))
        animation.toValue = NSValue(cgPoint: CGPoint(x: view.center.x + 10, y: view.center.y))
        view.layer.add(animation, forKey: "position")
    }
}
