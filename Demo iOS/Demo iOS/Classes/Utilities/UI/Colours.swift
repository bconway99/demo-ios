//
//  Colours.swift
//  Demo iOS
//
//  Created by Ben Conway on 20/02/2018.
//  Copyright © 2018 Ben Conway. All rights reserved.
//

import UIKit

class Colours: NSObject {

    // MARK: - Random
    
    class Random {

        static func getRandomColor(alpha: CGFloat) -> UIColor {
            
            let randomRed = CGFloat(drand48())
            let randomGreen = CGFloat(drand48())
            let randomBlue = CGFloat(drand48())
            return UIColor(red: randomRed, green: randomGreen, blue: randomBlue, alpha: alpha)
        }
    }
    
    // MARK: - Generics
    
    class Generics {
        
        static let red = UIColor.red
        static let cyan = UIColor.cyan
        static let blue = UIColor.blue
        static let grey = UIColor.gray
        static let clear = UIColor.clear
        static let green = UIColor.green
        static let white = UIColor.white
        static let black = UIColor.black
        static let brown = UIColor.brown
        static let purple = UIColor.purple
        static let orange = UIColor.orange
        static let yellow = UIColor.yellow
        static let magenta = UIColor.magenta
        static let darkGrey = UIColor.darkGray
        static let darkText = UIColor.darkText
        static let lightGrey = UIColor.lightGray
        static let lightText = UIColor.lightText
    }
    
    // MARK: - Custom
    
    class Custom {

        static let fbBlue = UIColor(displayP3Red: 68.0/255.0, green: 104.0/255.0, blue: 176.0/255.0, alpha: 1.0)
        static let black5 = UIColor(displayP3Red: 0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 0.5)
    }
}
