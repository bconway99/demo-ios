//
//  Strings.swift
//  Demo iOS
//
//  Created by Ben Conway on 20/02/2018.
//  Copyright © 2018 Ben Conway. All rights reserved.
//

import UIKit

class Strings: NSObject {

    // MARK: - General
    
    class General {
        
        static let camera: String = "camera"
        static let cancel: String = "cancel"
        static let contactUs: String = "contact us"
        static let error: String = "error"
        static let familyFriendly: String = "family friendly"
        static let food: String = "food"
        static let live: String = "live"
        static let logOut: String = "log out"
        static let music: String = "music"
        static let na: String = "N/A"
        static let ok: String = "ok"
        static let outdoorSeating: String = "outdoor seating"
        static let parking: String = "parking"
        static let petFriendly: String = "pet friendly"
        static let photoLibrary: String = "photo library"
        static let pool: String = "pool"
        static let profile: String = "profile"
        static let quizNight: String = "quiz night"
        static let settings: String = "settings"
        static let sport: String = "sport"
        static let stepFreeAccess: String = "step free access"
        static let tv: String = "TV"
        static let wifi: String = "WiFi"
    }
    
    // MARK: - Days of Week
    
    class DaysOfWeek {
        
        static let monday: String = "monday"
        static let tuesday: String = "tuesday"
        static let wednesday: String = "wednesday"
        static let thursday: String = "thursday"
        static let friday: String = "friday"
        static let saturday: String = "saturday"
        static let sunday: String = "sunday"
    }
    
    // MARK: - Alerts
    
    class Alerts {
        
        // Titles.
        
        class Titles {
            
            static let error: String = "error"
            static let locNotEnabled: String = "Location Not Enabled"
            static let chooseSource: String = "Choose Source"
            static let cameraNotFound: String = "Camera Not Found"
            static let photoLibraryNotFound: String = "Photo Library Not Found"
        }
        
        // Messages.
        
        class Messages {
            
            static let general: String = "Sorry there was an error, please try again."
            static let loginFailed: String = "Login failed, please try again."
            static let searchFailed: String = "There was an error finding your local pubs, please try again later."
            static let allowLocInstrc: String = "Your location services are not enabled, please allow them so we can provide you with a better experience."
            static let specifyDrink: String = "You must specify a drink for this search type."
            static let failedToDirect: String = "Failed to get directions."
            static let noDrinkPrices: String = "Something went wrong with getting the establishment's drink prices, please try again later."
            static let cameraAccess: String = "Please allow the device access to your camera via the device's settings."
            static let photoLibraryAccess: String = "Please allow the device access to your photo library via the device's settings."
            static let failedToPicture: String = "Failed to get picture."
        }
    }
    
    // MARK: - Search Types

    class SearchTypes {
        
        static let cheapestPint: String = "cheapest pint"
        static let cheapestDrink: String = "cheapest drink"
        static let cheapestType: String = "cheapest type"
        static let closestPub: String = "closest pub"
    }
}
