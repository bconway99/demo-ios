//
//  StringUtil.swift
//  Demo iOS
//
//  Created by Ben Conway on 15/05/2018.
//  Copyright © 2018 Ben Conway. All rights reserved.
//

import UIKit

class StringUtil: NSObject {

    // Decode the base64 string, return nil if it fails.
    static func base64Decode(base64Encoded: String) -> String? {
        
        let decodedData = Data(base64Encoded: base64Encoded)!
        let decodedStr = String(data: decodedData, encoding: .utf8)
        return decodedStr
    }
}
