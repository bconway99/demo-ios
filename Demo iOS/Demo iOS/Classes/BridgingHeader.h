//
//  BridgingHeader.h
//  Demo iOS
//
//  Created by Ben Conway on 20/02/2018.
//  Copyright © 2018 Ben Conway. All rights reserved.
//

#ifndef BridgingHeader_h
#define BridgingHeader_h

// Use this class to import Objective-C frameworks / CocoaPods so that they can be used in Swift.

#import <FBSDKCoreKit/FBSDKCoreKit.h>

#endif /* BridgingHeader_h */
