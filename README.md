# Demo iOS

A brief example of an iOS application that I previously worked on. The application was built to aid users in finding the best drinking establishments in the London area, including pubs, bars and restaurants. The project didn't go live and I have changed the project name as well as hid some of the domains and API keys.
